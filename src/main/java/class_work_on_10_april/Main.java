package class_work_on_10_april;

import stream_api.model.Group;
import stream_api.model.Name;
import stream_api.model.Sex;
import stream_api.model.StudentInGroup;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) throws IOException {
        StudentInGroup student1 = new StudentInGroup(Name.MaleName.MARAT.toString(), Optional.of(Group.GROUP_5), 19, Sex.MALE);
        StudentInGroup student2 = new StudentInGroup(Name.MaleName.EDGAR.toString(), Optional.of(Group.GROUP_5), 18, Sex.MALE);
        StudentInGroup student3 = new StudentInGroup(Name.MaleName.MISHA.toString(), Optional.of(Group.GROUP_5), 18, Sex.MALE);
        StudentInGroup student4 = new StudentInGroup(Name.MaleName.RADIMIR.toString(), Optional.of(Group.GROUP_5), 18, Sex.MALE);

        ArrayList<StudentInGroup> students = new ArrayList<>();

        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);

        addStudentListToFile("serial/dest", students);
        List<StudentInGroup> deserialized = readStudentsFromFile("serial/dest");

        System.out.println(deserialized);
    }

    private static void addStudentListToFile(String filename, List<StudentInGroup> students) throws IOException {
        FileOutputStream fos = new FileOutputStream(filename);
        DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));

        for (StudentInGroup student : students) {
            System.out.println(student);
            outStream.writeUTF(student.getName());
            outStream.writeInt(student.getAge());
            outStream.writeUTF(student.getSex().toString());
            outStream.writeUTF(student.getGroup().orElse(Group.NONE).toString());
        }

        outStream.close();
    }

    private static List<StudentInGroup> readStudentsFromFile(String filename) throws IOException {
        FileInputStream fis = new FileInputStream(filename);
        DataInputStream reader = new DataInputStream(fis);
        ArrayList<StudentInGroup> arrayList = new ArrayList<>();
        while (reader.available() > 0) {
            String name = reader.readUTF();
            int age = reader.readInt();
            Sex sex = Sex.valueOf(reader.readUTF());
            Group group = Group.valueOf(reader.readUTF());
            group = group.equals("NULLPTR") ? null : group;


            arrayList.add(new StudentInGroup(name, Optional.of(group), age, sex));
        }
        return arrayList;
    }
}