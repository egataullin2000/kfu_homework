package stream_api;

import stream_api.model.Group;
import stream_api.model.Name;
import stream_api.model.Sex;
import stream_api.model.StudentInGroup;

import java.util.Optional;
import java.util.Random;

public class Randomizer {

    public static StudentInGroup randomStudent() {
        Sex sex = Randomizer.randomSex();
        return new StudentInGroup(
                Randomizer.randomName(sex), Randomizer.randomGroup(), Randomizer.randomAge(), sex
        );
    }

    public static String randomName(Sex sex) {
        if (sex == Sex.MALE) {
            Name.MaleName[] maleNames = Name.MaleName.values();
            return maleNames[new Random().nextInt(maleNames.length)].toString();
        } else if (sex == Sex.FEMALE) {
            Name.FemaleName[] femaleNames = Name.FemaleName.values();
            return femaleNames[new Random().nextInt(femaleNames.length)].toString();
        }

        return null;
    }

    public static Optional<Group> randomGroup() {
        boolean hasGroup = new Random().nextBoolean();
        if (hasGroup) {
            Group[] groups = Group.values();
            return Optional.of(groups[new Random().nextInt(groups.length)]);
        } else return Optional.empty();
    }

    /**
     * Generates random age from 16 to 20
     */
    public static int randomAge() {
        return 16 + new Random().nextInt(5);
    }

    public static Sex randomSex() {
        Sex[] sexes = Sex.values();
        return sexes[new Random().nextInt(sexes.length)];
    }

}
