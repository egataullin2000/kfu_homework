package stream_api;

import stream_api.model.StudentInGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task14 extends Task13 {

    public static void main(String[] args) {
        System.out.println(finalDistr());
    }

    public static Supplier<Stream<StudentInGroup>> distrib() {
        List<StudentInGroup> ar = getSupStream()
                .get()
                .limit(10_000)
                .collect(Collectors.toCollection(ArrayList::new));

        return ar::stream;
    }

    public static void distrHelper(StudentInGroup student) {
        if (!student.getGroup().isPresent()) {
            student.setGroup(Randomizer.randomGroup());
        }
    }

    public static List finalDistr() {
        return distrib()
                .get()
                .peek(Task14::distrHelper)
                .collect(Collectors.toList());

    }

}
