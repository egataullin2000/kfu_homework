package stream_api;

import stream_api.model.StudentInGroup;

import java.util.List;
import java.util.stream.Collectors;

public class Test1 {

    public static void main(String[] args) {
        int studentsCount = 20;
        List<StudentInGroup> students = StudentsGenerator.generate()
                .distinct()
                .limit(studentsCount)
                .sorted()
                .collect(Collectors.toList());
        students.forEach(System.out::println);
    }

}