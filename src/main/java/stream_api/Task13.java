package stream_api;

import stream_api.model.Group;
import stream_api.model.StudentInGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task13 {

    public static void main(String[] args) {
        System.out.println(mapNameStr());
    }

    public static Map mapName1() {
        return getSupStream()
                .get()
                .limit(100)
                .collect(Collectors.groupingBy(StudentInGroup::getName));
    }

    public static Map mapName2() {
        return getSupStream()
                .get()
                .parallel()
                .limit(100)
                .collect(Collectors.groupingBy(StudentInGroup::getName));

    }

    public static Map mapGr1() {
        return getSupStream()
                .get()
                .limit(100)
                .collect(
                        Collectors.toMap(
                                a -> a.getGroup().orElse(Group.NONE),
                                b -> new ArrayList<>(Arrays.asList(b)),
                                (x, y) -> {
                                    x.addAll(y);
                                    return x;
                                }
                        )
                );
    }

    public static Map mapGr2() {
        return getSupStream()
                .get()
                .parallel()
                .limit(100)
                .collect(
                        Collectors.toMap(
                                a -> a.getGroup().orElse(Group.NONE),
                                b -> new ArrayList<>(Arrays.asList(b)),
                                (x, y) -> {
                                    x.addAll(y);
                                    return x;
                                }
                        )
                );
    }

    public static Map mapNameStr() {
        return getSupStream()
                .get()
                .limit(100)
                .collect(
                        Collectors.toMap(
                                StudentInGroup::getName,
                                x -> new ArrayList(Arrays.asList(x.getGroup())),
                                (a, b) -> {
                                    a.addAll(b);
                                    return a;
                                }
                        )
                );
    }

    public static Map mapNameStr2() {
        return getSupStream()
                .get()
                .parallel()
                .limit(100)
                .collect(
                        Collectors.toMap(
                                StudentInGroup::getName,
                                x -> new ArrayList(Arrays.asList(x.getGroup())),
                                (a, b) -> {
                                    a.addAll(b);
                                    return a;
                                }
                        )
                );
    }

    public static Map mapGroupStr() {
        return getSupStream()
                .get()
                .limit(100)
                .collect(
                        Collectors.toMap(
                                a -> a.getGroup().orElse(Group.NONE),
                                b -> new ArrayList<>(Arrays.asList(b.getName())),
                                (x, y) -> {
                                    x.addAll(y);
                                    return x;
                                }
                        )
                );
    }

    public static Map mapGroupStr2() {
        return getSupStream()
                .get()
                .parallel()
                .limit(100)
                .collect(
                        Collectors.toMap(
                                a -> a.getGroup().orElse(Group.NONE),
                                b -> new ArrayList<>(Arrays.asList(b.getName())),
                                (x, y) -> {
                                    x.addAll(y);
                                    return x;
                                }
                        )
                );
    }

    public static Supplier<Stream<StudentInGroup>> getSupStream() {
        return (StudentsGenerator::generate);
    }

}
