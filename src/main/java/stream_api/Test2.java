package stream_api;

import stream_api.model.StudentInGroup;

import java.time.LocalDate;
import java.util.IntSummaryStatistics;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Test2 {

    public static void main(String[] args) {
        LocalDate date = LocalDate.now();
        int thisYear = date.getYear();
        Supplier<Stream<StudentInGroup>> streamSupplier = StudentsGenerator::generate;
        int limit = 10_000;

        long startTime = System.nanoTime();
        IntSummaryStatistics statistics1 = streamSupplier.get()
                .limit(limit)
                .mapToInt(e -> thisYear - e.getAge())
                .summaryStatistics();
        long time1 = System.nanoTime() - startTime;

        startTime = System.nanoTime();
        IntSummaryStatistics statistics2 = streamSupplier.get().
                parallel()
                .limit(limit)
                .mapToInt(e -> thisYear - e.getAge())
                .summaryStatistics();
        long time2 = System.nanoTime() - startTime;

        startTime = System.nanoTime();
        IntSummaryStatistics statistics3 = streamSupplier.get()
                .limit(limit)
                .parallel()
                .mapToInt(e -> thisYear - e.getAge())
                .summaryStatistics();
        long time3 = System.nanoTime() - startTime;

        startTime = System.nanoTime();
        IntSummaryStatistics statistics4 = streamSupplier.get()
                .limit(limit)
                .mapToInt(StudentInGroup::getAge)
                .parallel().map(e -> thisYear - e)
                .summaryStatistics();
        long time4 = System.nanoTime() - startTime;

        System.out.println(statistics1);
        System.out.println("Statistics1:\n" + " time: " + time1 / 1_000_000d + " ms.\n");

        System.out.println(statistics2);
        System.out.println("Statistics2:\n" + " time: " + time2 / 1_000_000d + " ms.\n");

        System.out.println(statistics3);
        System.out.println("Statistics3:\n" + " time: " + time3 / 1_000_000d + " ms.\n");

        System.out.println(statistics4);
        System.out.println("Statistics4:\n" + " time: " + time4 / 1_000_000d + " ms.\n");
    }

}
