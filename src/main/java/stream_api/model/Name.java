package stream_api.model;

public class Name {
    public enum MaleName {
        EDGAR,
        MARAT,
        MISHA,
        RADIMIR
    }

    public enum FemaleName {
        LIYA,
        DASHA,
        KAMILLA,
        NASTYA,
        ALIYA
    }
}
