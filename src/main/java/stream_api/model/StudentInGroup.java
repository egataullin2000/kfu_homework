package stream_api.model;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class StudentInGroup implements Comparable<StudentInGroup> {

    static {
        System.out.println("StudentInGroup loaded");
    }

    private Optional<Group> group;
    private String name;
    private int age;
    private Sex sex;

    public StudentInGroup(String name, Optional<Group> group, int age, Sex sex) {
        this.name = name;
        this.group = group;
        this.age = age;
        this.sex = sex;
    }

    public Optional<Group> getGroup() {
        return group;
    }

    public void setGroup(Optional<Group> group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(@NotNull StudentInGroup student) {
        return name.compareTo(student.name);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof StudentInGroup) {
            StudentInGroup student = (StudentInGroup) o;
            return student.name.equals(name) && student.group.equals(group) && student.age == age;
        } else return false;
    }

    @Override
    public String toString() {
        return "StudentInGroup{" +
                "group=" + group.orElse(null) +
                ",name='" + name + '\'' +
                ",age=" + age +
                ",sex=" + sex +
                '}';
    }

}
