package stream_api.model;

/**
 * Пол (мужской/женский)
 */
public enum Sex {
    MALE,
    FEMALE
}