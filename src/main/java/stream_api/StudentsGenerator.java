package stream_api;

import stream_api.model.StudentInGroup;

import java.util.stream.Stream;

public class StudentsGenerator {

    public static Stream<StudentInGroup> generate() {
        return Stream.generate(Randomizer::randomStudent);
    }

}
