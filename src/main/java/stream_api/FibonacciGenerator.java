package stream_api;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FibonacciGenerator {
    public static List<Integer> generate(int n) {
        return Stream.iterate(new int[]{1, 1}, a -> new int[]{a[1], a[0] + a[1]})
                .limit(n)
                .map(i -> i[0])
                .collect(Collectors.toList());
    }

    public static int product(int n) {
        return generate(n).stream().reduce((x, y) -> x * y).orElse(0);
    }
}
