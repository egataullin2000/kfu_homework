package stream_api;

import stream_api.model.StudentInGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task15 {

    public static void main(String[] args) {
        new Task15().finMeth();
    }

    public Supplier<Stream<StudentInGroup>> getSupStream() {
        return StudentsGenerator::generate;
    }

    public Supplier<Stream<StudentInGroup>> distrib() {
        List<StudentInGroup> students = getSupStream()
                .get()
                .limit(10000)
                .collect(Collectors.toCollection(ArrayList::new));

        return students::stream;
    }

    public void distrHelper(StudentInGroup s) {
        String[] groupNum = {"11-801", "11-804", "11-805"};
        if (!s.getGroup().isPresent()) s.setGroup(Randomizer.randomGroup());
    }

    public List finalDistr() {
        return distrib()
                .get()
                .peek(new Task15()::distrHelper)
                .collect(Collectors.toList());
    }

    public long SomeMeth() {
        long time1 = System.nanoTime();
        new Task15().finalDistr();
        long time2 = System.nanoTime();

        return time2 - time1;
    }

    public void finMeth() {
        Supplier<Stream<Long>> sup = () -> Stream.generate(() -> (new Task15().SomeMeth()))
                .limit(100);

        System.out.println(sup.get().collect(Collectors.summarizingLong(a -> a)));
        System.out.println(sup.get().parallel().collect(Collectors.summarizingLong(b -> b)));
    }

}
