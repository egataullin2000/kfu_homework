package control_work_15_05.task2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataWriter {

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Edgar", "Gataullin", 19, true));
        people.add(new Person("Marat", "Shigabutdinov", 19, true));
        people.add(new Person("Mikhail", "Dmitriev", 18, true));
        people.add(new Person("Ruslan", "Korchenov", 19, true));
        people.add(new Person("Radimir", "Mamedov", 19, true));
        write(people);
    }

    private static void write(List<Person> people) {
        try (FileOutputStream fileOutputStream = new FileOutputStream("data.txt")) {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            DataOutputStream dataOutputStream = new DataOutputStream(bufferedOutputStream);
            for (Person person : people) {
                String s = person + "\n";
                dataOutputStream.write(s.getBytes());
            }
            dataOutputStream.close();
            bufferedOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
