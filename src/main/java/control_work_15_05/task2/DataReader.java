package control_work_15_05.task2;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DataReader {

    public static void main(String[] args) {
        List<Person> people = read();
        for (Person person : people) {
            System.out.println(person);
        }
    }

    private static List<Person> read() {
        List<Person> people = null;
        try (Scanner scanner = new Scanner(new FileReader("data.txt"))) {
            people = new ArrayList<>();
            while (scanner.hasNext()) {
                people.add(new Person(scanner.next(), scanner.next(), scanner.nextInt(), scanner.nextBoolean()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return people;
    }

}
