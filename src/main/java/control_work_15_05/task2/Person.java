package control_work_15_05.task2;

public class Person {

    private String name, surname;
    private int age;
    private boolean isMale;

    public Person(String name, String surname, int age, boolean isMale) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isMale = isMale;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public boolean isMale() {
        return isMale;
    }

    @Override
    public String toString() {
        return name + " " + surname + " " + age + " " + isMale;
    }

}
