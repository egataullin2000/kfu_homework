package control_work_15_05.task3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main implements Runnable {

    private String fileName;

    @Override
    public void run() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            stringBuilder.append(new Random().nextInt(300));
        }
        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
