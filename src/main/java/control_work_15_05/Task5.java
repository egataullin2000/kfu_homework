package control_work_15_05;

import control_work_15_05.task2.Person;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Task5 {

    public static void main(String[] args) {
        var people = new ArrayList<Person>();
        people.add(new Person("Edgar", "Gataullin", 19, true));
        people.add(new Person("Marat", "Shigabutdinov", 19, true));
        people.add(new Person("Mikhail", "Dmitriev", 18, true));
        people.add(new Person("Ruslan", "Korchenov", 19, true));
        people.add(new Person("Radimir", "Mamedov", 19, true));
        writeZip(people);
    }

    private static void writeZip(List<Person> people) {
        var stringBuilder = new StringBuilder();
        for (var person : people) {
            stringBuilder.append(person.getName())
                    .append(" ").append(person.getSurname())
                    .append(" ").append(person.getAge())
                    .append(" ").append(person.isMale())
                    .append("\n");
        }

        try (var zout = new ZipOutputStream(new FileOutputStream("data.zip"))) {
            var zipEntry = new ZipEntry("data.txt");
            zout.putNextEntry(zipEntry);
            var bData = stringBuilder.toString().getBytes();
            zout.write(bData, 0, bData.length);
            zout.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
