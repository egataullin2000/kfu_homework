package control_work_15_05.task1;

import stream_api.model.Group;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataWriter {

    public static void main(String[] args) {
        List<Student> students = new ArrayList<>(5);
        Group group = Group.GROUP_5;
        students.add(new Student("Edgar", "Gataullin", group));
        students.add(new Student("Mikhail", "Dmitriev", group));
        students.add(new Student("Marat", "Shigabutdinov", group));
        students.add(new Student("Ruslan", "Korchenov", group));
        students.add(new Student("Radimir", "Mamedov", group));
        write(students);
    }

    private static void write(List<Student> data) {
        try (FileWriter fileWriter = new FileWriter("data.txt")) {
            fileWriter.write(data.get(0).getGroup().toString());
            fileWriter.write("\n");
            for (Student student : data) {
                fileWriter.write(student.getName());
                fileWriter.write(" ");
                fileWriter.write(student.getSurname());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
