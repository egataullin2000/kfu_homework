package control_work_15_05.task1;

import stream_api.model.Group;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DataReader {

    public static void main(String[] args) {
        List<Student> students = readStudents();
        for (Student student : students) {
            System.out.println(student);
        }
    }

    private static Group readGroup(String fileName) {
        Group group = Group.NONE;
        try {
            Scanner scanner = new Scanner(new FileReader(fileName));
            group = Group.valueOf(scanner.next());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return group;
    }

    private static List<Student> readStudents() {
        List<Student> data = null;
        try (Scanner scanner = new Scanner(new FileReader("data.txt"))) {
            Group group = Group.valueOf(scanner.next());
            data = new ArrayList<>();
            while (scanner.hasNext()) {
                Student student = new Student(
                        scanner.next(),
                        scanner.next(),
                        group
                );
                data.add(student);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return data;
    }

}
