package aisd;

import aisd.stack.StackMergeSorter;

public class MergeTester {

    public static void main(String[] args) {
//        int max=100;
//        MergingSorting mergingSorting =new MergingSorting(max);
//
//        mergingSorting.insert(64);
//        mergingSorting.insert(21);
//        mergingSorting.insert(33);
//        mergingSorting.insert(70);
//        mergingSorting.insert(12);
//        mergingSorting.insert(85);
//        mergingSorting.insert(44);
//        mergingSorting.insert(3);
//        mergingSorting.insert(99);
//        mergingSorting.insert(0);
//        mergingSorting.insert(108);
//        mergingSorting.insert(36);
//
//        mergingSorting.display();
//        mergingSorting.sort();
//        mergingSorting.display();

        int maxArray = 100;
        int maxStack = 10000;
        StackMergeSorter stackMergeSorter = new StackMergeSorter(maxArray, maxStack);

        stackMergeSorter.insert(64);
        stackMergeSorter.insert(21);
        stackMergeSorter.insert(33);
        stackMergeSorter.insert(70);
        stackMergeSorter.insert(12);
        stackMergeSorter.insert(85);
        stackMergeSorter.insert(44);
        stackMergeSorter.insert(3);
        stackMergeSorter.insert(99);
        stackMergeSorter.insert(0);
        stackMergeSorter.insert(108);
        stackMergeSorter.insert(36);
        stackMergeSorter.display();
        stackMergeSorter.mergeSort();
        stackMergeSorter.display();
    }

}
