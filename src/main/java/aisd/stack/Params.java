package aisd.stack;

public class Params {

    private int lowerBound;
    private int upperBound;
    private int returnAddress;

    public Params(int lowerBound, int upperBound, int ra) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.returnAddress = ra;
    }

    public int getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    public int getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    public int getReturnAddress() {
        return returnAddress;
    }

    public void setReturnAddress(int returnAddress) {
        this.returnAddress = returnAddress;
    }
}
