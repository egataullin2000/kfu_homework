package aisd.stack;

public class StackMergeSorter {

    private MergingStack mergingStack;
    private int codePart;
    private Params currentParams;

    private int arraySize;
    private int nElems;
    private int stackSize;

    private long[] theArray;
    private long[] workSpace;
    private int lowerBound;
    private int upperBound;

    public StackMergeSorter(int arrayCapacity, int stackCapacity) {
        arraySize = arrayCapacity;
        stackSize = stackCapacity;
        theArray = new long[arraySize];
    }

    public void insert(long value) {
        theArray[nElems++] = value;
    }

    public void display() {
        for (int j = 0; j < nElems; j++) System.out.print(theArray[j] + " ");
        System.out.println();
    }

    public void mergeSort() {
        workSpace = new long[arraySize];
        recMerging(workSpace, stackSize);
    }

    private void recMerging(long[] workspace, int stackSize) {
        mergingStack = new MergingStack(stackSize);
        codePart = 1;
        while (step() == false) ;
    }

    private void merge(long[] workSpace, int lowPtr, int highPtr, int upperBound) {
        int j = 0;
        int lowerBound = lowPtr;
        int mid = highPtr - 1;
        int n = upperBound - lowerBound + 1;

        while (lowPtr <= mid && highPtr <= upperBound)
            if (theArray[lowPtr] < theArray[highPtr])
                workSpace[j++] = theArray[lowPtr++];
            else
                workSpace[j++] = theArray[highPtr++];

        while (lowPtr <= mid)
            workSpace[j++] = theArray[lowPtr++];
        while (highPtr <= upperBound)
            workSpace[j++] = theArray[highPtr++];
        for (j = 0; j < n; j++) {
            theArray[lowerBound + j] = workSpace[j];
        }
    }

    private boolean step() {
        switch (codePart) {
            case 1:
                currentParams = new Params(0, nElems - 1, 6);
                mergingStack.push(currentParams);
                codePart = 2;
                break;
            case 2:
                currentParams = mergingStack.peek();
                System.out.println("Stack call with " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                if (currentParams.getLowerBound() == currentParams.getUpperBound()) {
                    //todo or not todo?
                    System.out.println("Processing stack's local deep: " + currentParams.getLowerBound() + "/" + currentParams.getUpperBound());
                    codePart = 5;
                    System.out.println("Stack back->5");
                } else {
                    codePart = 3;
                    System.out.println("Stack deep->3");
                }
                break;
            case 3:
                Params newParams = new Params(currentParams.getLowerBound(), (currentParams.getUpperBound() + currentParams.getLowerBound()) / 2, 4);
                Params newParams2 = new Params((currentParams.getUpperBound() + currentParams.getLowerBound()) / 2 + 1, currentParams.getUpperBound(), 44);
                //todo the correct parametrization on initializing!!!
                mergingStack.display("Before 3 " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                mergingStack.push(newParams2);
                mergingStack.display("Middle 3 " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                mergingStack.push(newParams);
                mergingStack.display("After 3 " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                codePart = 2;
                break;
            case 4:
                mergingStack.display("Before 4 " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                currentParams = mergingStack.peek();
                mergingStack.display("After 4 " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                merge(workSpace, currentParams.getLowerBound(), (currentParams.getUpperBound() + currentParams.getLowerBound()) / 2 + 1, currentParams.getUpperBound());
                display();
                codePart = 2;
                break;
            case 44:
                mergingStack.display("Before 44 " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                currentParams = mergingStack.peek();
                mergingStack.display("After 44 " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                merge(workSpace, currentParams.getLowerBound(), (currentParams.getUpperBound() + currentParams.getLowerBound()) / 2 + 1, currentParams.getUpperBound());
                display();
                codePart = 5;
                break;
            case 5:
                mergingStack.display("Before 5 pop " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                //mergingStack.display("Before 5 pop 2 "+currentParams.lowerBound+"-"+currentParams.upperBound);
                //mergingStack.pop();
                currentParams = mergingStack.peek();
                codePart = currentParams.getReturnAddress();
                mergingStack.pop();
                mergingStack.display("After 5 pop " + currentParams.getLowerBound() + "-" + currentParams.getUpperBound());
                //codePart = 3;
                break;
            case 6:
                return true;
        }
        return false;
    }

}
