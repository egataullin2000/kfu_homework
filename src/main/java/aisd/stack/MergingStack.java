package aisd.stack;

public class MergingStack {

    private int capacity;
    private Params[] stackArray;
    private int top;

    public MergingStack(int capacity) {
        this.capacity = capacity;
        this.stackArray = new Params[capacity];
        top = -1;
    }

    public void push(Params p) {
        stackArray[++top] = p;
    }

    public Params pop() {
        Params params = stackArray[top--];
        stackArray[top + 1] = null;
        return params;
    }

    public Params peek() {
        return stackArray[top];
    }

    public void display(String message) {
        System.out.print(message + ": ");
        for (int i = 0; i <= top; i++) {
            System.out.print(stackArray[i].getLowerBound() + "/" + stackArray[i].getUpperBound() + " ");
        }
        System.out.println();
    }

}
