package aisd;

public class MergeSort {

    private long[] array;
    private int size;
    private int st = 0;

    public MergeSort(int capacity) {
        array = new long[capacity];
        size = 0;
    }

    public void insert(long value) {
        array[size++] = value;
    }

    public void display() {
        for (long i : array) System.out.print(i + " ");
        System.out.println();
    }

    public void sort() {
        long[] workSpace = new long[size];
        recMergeSort(workSpace, 0, size - 1);
    }

    private void recMergeSort(long[] workSpace, int first, int last) {
        for (int i = 0; i <= st; i++) System.out.print(" ");
        st++;
        System.out.println("Borders: " + first + "-" + last);
        if (first == last) {
            st--;
            display();
            return;
        } else {
            int mid = (first + last) / 2;
            recMergeSort(workSpace, first, mid);
            recMergeSort(workSpace, mid + 1, last);
            merge(workSpace, first, mid + 1, last);
        }
        st--;
        display();
    }

    private void merge(long[] workSpace, int lowPtr, int highPtr, int upperBound) {
        int j = 0;
        int lowerBound = lowPtr;
        int mid = highPtr - 1;
        int n = upperBound - lowerBound + 1;

        while (lowPtr <= mid && highPtr <= upperBound) {
            if (array[lowPtr] < array[highPtr]) {
                workSpace[j++] = array[lowPtr++];
            } else {
                workSpace[j++] = array[highPtr++];
            }
        }

        while (lowPtr <= mid) workSpace[j++] = array[lowPtr++];
        while (highPtr <= upperBound) workSpace[j++] = array[highPtr++];
        for (j = 0; j < n; j++) array[lowerBound + j] = workSpace[j];
    }

}
