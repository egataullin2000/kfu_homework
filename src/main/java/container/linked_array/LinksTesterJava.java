package container.linked_array;

import container.model.StudentJava;

import java.util.Date;

public class LinksTesterJava {

    public static void main(String[] args) {
        System.out.println("Testing LinksContainer:");
        LinksContainerJava<Long> linksContainer = new LinksContainerJava<>(20L);
        linksContainer.insert(10L);
        linksContainer.insert(30L);
        System.out.println("\t" + linksContainer);
        linksContainer.removeFirst();
        System.out.println("\t" + linksContainer);

        System.out.println("Testing SortedLinksContainer");
        SortedLinksContainerJava<Long> sortedLinksContainer = new SortedLinksContainerJava<>(3L);
        sortedLinksContainer.insert(1L);
        System.out.println("\t" + sortedLinksContainer);
        //sortedLinksContainer.insert(20)
        //sortedLinksContainer.insert(1)
        sortedLinksContainer.removeFirst();
        System.out.println("\t" + sortedLinksContainer);

        System.out.println("Testing BidirectionalLinksContainer");
        BidirectionalLinkContainerJava<Long> bidirectionalLinkContainer =
                new BidirectionalLinkContainerJava<>(3L);
        bidirectionalLinkContainer.insert(1L);
        System.out.println("\t" + bidirectionalLinkContainer);
        bidirectionalLinkContainer.removeFirst();
        System.out.println("\t" + bidirectionalLinkContainer);

        System.out.println("Testing StudentLinksContainer");
        StudentLinksContainerJava studentLinksContainer = new StudentLinksContainerJava(
                new StudentJava("1", "2", "3", new Date("10 April 2000"))
        );
        studentLinksContainer.insert(new StudentJava("2", "3", "4", new Date("17 April 2000")));
        System.out.println("\t" + studentLinksContainer);
        studentLinksContainer.remove("2", "3", "4");
        System.out.println("\t" + studentLinksContainer);
    }

}
