package container.linked_array;

import container.model.LinkJava;

public class LinksContainerJava<E extends Comparable<E>> {

    protected LinkJava<E> starterLink;

    public LinksContainerJava(E firstValue) {
        starterLink = new LinkJava<>(firstValue);
    }

    public void insert(E item) {
        LinkJava<E> newLink = new LinkJava<>(item);
        if (starterLink == null) {
            starterLink = newLink;
        } else {
            LinkJava<E> currentLink = starterLink;
            while (currentLink.hasNext()) {
                currentLink = currentLink.getNextLink();
            }
            currentLink.setNextLink(newLink);
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(starterLink.toString()).append(" ");
        LinkJava<E> currentLink = starterLink;
        while (currentLink != null && currentLink.hasNext()) {
            currentLink = currentLink.getNextLink();
            stringBuilder.append(currentLink.getValue().toString()).append(" ");
        }

        return stringBuilder.toString();
    }

    public void removeFirst() {
        if (starterLink != null) {
            starterLink = starterLink.getNextLink();
        }
    }

}
