package container.linked_array;

import container.model.BidirectionalLinkJava;

public class BidirectionalLinkContainerJava<E extends Comparable<E>> {

    protected BidirectionalLinkJava<E> starterLink;
    protected BidirectionalLinkJava<E> lastLink;

    public BidirectionalLinkContainerJava(E firstValue) {
        starterLink = new BidirectionalLinkJava<>(firstValue);
        lastLink = starterLink;
    }

    public void insert(E value) {
        BidirectionalLinkJava<E> newLink = new BidirectionalLinkJava<>(value);
        if (starterLink == null) {
            starterLink = newLink;
            lastLink = starterLink;
        } else if (newLink.getValue().compareTo(starterLink.getValue()) <= 0) {
            newLink.setNextLink(starterLink);
            starterLink.setPrevLink(newLink);
            starterLink = newLink;
        } else {
            BidirectionalLinkJava<E> currentLink = starterLink;
            do {
                if (newLink.getValue().compareTo(currentLink.getValue()) >= 0) {
                    newLink.setPrevLink(currentLink);
                    newLink.setNextLink(currentLink.getNextLink());
                    if (newLink.getNextLink() == null) lastLink = newLink;
                    currentLink.setNextLink(newLink);
                    break;
                }
                currentLink = currentLink.getNextLink();
            } while (currentLink.hasNext());
        }
    }

    public void removeFirst() {
        if (starterLink != null && starterLink.hasNext()) {
            starterLink = starterLink.getNextLink();
        } else {
            starterLink = null;
        }
    }

    public void remove(E value) {
        BidirectionalLinkJava<E> currentLink = starterLink;
        while (currentLink.getValue() != value) {
            currentLink = currentLink.getNextLink();
        }
        if (currentLink.getNextLink() == null) lastLink = currentLink.getPrevLink();
        currentLink.getPrevLink().setNextLink(currentLink.getNextLink());
        currentLink.getNextLink().setPrevLink(currentLink.getPrevLink());
    }

    public void display(boolean fromStart) {
        if (fromStart) {
            System.out.println(this);
        } else {
            System.out.println(toStringWithRevers());
        }
    }

    private String toStringWithRevers() {
        StringBuilder stringBuilder = new StringBuilder();
        BidirectionalLinkJava<E> currentLink = lastLink;
        stringBuilder.append(currentLink.getValue()).append(" ");
        while (currentLink.hasPrev()) {
            currentLink = currentLink.getPrevLink();
            stringBuilder.append(currentLink.getValue()).append(" ");
        }

        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        BidirectionalLinkJava<E> currentLink = starterLink;
        stringBuilder.append(currentLink.getValue()).append(" ");
        while (currentLink.hasNext()) {
            currentLink = currentLink.getNextLink();
            stringBuilder.append(currentLink.getValue()).append(" ");
        }

        return stringBuilder.toString();
    }

}
