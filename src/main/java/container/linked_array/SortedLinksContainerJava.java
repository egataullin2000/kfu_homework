package container.linked_array;

import container.model.LinkJava;

public class SortedLinksContainerJava<E extends Comparable<E>> extends LinksContainerJava<E> {

    public SortedLinksContainerJava(E firstValue) {
        super(firstValue);
    }

    @Override
    public void insert(E item) {
        LinkJava<E> newLink = new LinkJava<>(item);
        if (starterLink == null) {
            starterLink = newLink;
        } else if (newLink.getValue().compareTo(starterLink.getValue()) <= 0) {
            newLink.setNextLink(starterLink);
            starterLink = newLink;
        } else {
            LinkJava<E> currentLink = starterLink;
            do {
                if (newLink.getValue().compareTo(currentLink.getValue()) >= 0) {
                    newLink.setNextLink(currentLink.getNextLink());
                    currentLink.setNextLink(newLink);
                    break;
                }
                currentLink = currentLink.getNextLink();
            } while (currentLink.hasNext());
        }
    }

}
