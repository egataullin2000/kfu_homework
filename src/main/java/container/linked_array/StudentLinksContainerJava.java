package container.linked_array;

import container.model.LinkJava;
import container.model.StudentJava;

public class StudentLinksContainerJava extends SortedLinksContainerJava<StudentJava> {

    public StudentLinksContainerJava(StudentJava firstStudent) {
        super(firstStudent);
    }

    public void remove(String surname, String name, String patronymic) {
        StudentJava student = new StudentJava(surname, name, patronymic);
        if (starterLink.getValue() == student) {
            starterLink = starterLink.getNextLink();
        } else {
            LinkJava<StudentJava> currentLink = starterLink;
            while (!currentLink.getNextLink().getValue().equals(student)) {
                currentLink = currentLink.getNextLink();
            }
            currentLink.setNextLink(currentLink.getNextLink().getNextLink());
        }
    }

}
