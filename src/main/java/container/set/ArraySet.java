package container.set;

import container.Displayable;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class ArraySet implements Set<Object>, Displayable {

    private int lastIndex;
    private Object[] array;

    public ArraySet(int capacity) {
        array = new Object[capacity];
        lastIndex = -1;
    }

    @Override
    public int size() {
        return lastIndex + 1;
    }

    @Override
    public boolean isEmpty() {
        return lastIndex == -1;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    /**
     * Returns the index of chosen object.
     * If this object is not presented returns -1.
     */
    public int indexOf(Object o) {
        for (int i = 0; i <= lastIndex; i++) {
            if (array[i].equals(o)) return i;
        }

        return -1;
    }

    @Override
    public Iterator<Object> iterator() {
        return new Iterator<Object>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < lastIndex;
            }

            @Override
            public Object next() {
                return array[index++];
            }
        };
    }

    @Override
    public Object[] toArray() {
        return array;
    }

    @Override
    public boolean add(Object item) {
        if (!contains(item)) {
            if (lastIndex == array.length - 1) throw new IllegalStateException("Capacity limit reached");
            array[++lastIndex] = item;
            return true;
        } else return false;
    }

    @Override
    public boolean remove(Object item) {
        return remove(indexOf(item));
    }

    public boolean remove(int index) {
        if (index == -1) return false;
        if (index < 0) throw new IllegalStateException("Index can not be negative");
        if (index > lastIndex) throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
        System.arraycopy(array, index + 1, array, index, array.length - index - 1);
        array[lastIndex--] = null;

        return true;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean modified = false;
        for (Object element : c) {
            if (add(element)) modified = true;
        }

        return modified;
    }

    @Override
    public void clear() {
        for (int i = 0; i <= lastIndex; i++) {
            array[i] = null;
        }
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean modified = true;
        for (Object element : c) {
            if (!remove(element)) modified = false;
        }

        return modified;
    }

    @Override
    public boolean retainAll(Collection c) {
        boolean modified = false;
        for (int i = 0; i <= lastIndex; i++) {
            if (!c.contains(array[i])) {
                remove(array[i]);
                modified = true;
            }
        }

        return modified;
    }

    @Override
    public boolean containsAll(Collection c) {
        for (Object element : c) {
            if (!contains(element)) return false;
        }

        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object[] toArray(Object[] a) {
        int size = size();
        if (a.length < size) a = new Object[size];
        System.arraycopy(array, 0, a, 0, size);

        return a;
    }

    @Override
    public void display() {
        for (int i = 0; i <= lastIndex; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

}
