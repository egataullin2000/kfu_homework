package container.set;

import java.util.Arrays;

public class SetTester {

    public static void main(String[] args) {
        LinkedSet set = new LinkedSet();
        set.addAll(Arrays.asList(1, 6, 3, 5, 2));
        set.remove(1);
        set.display();
        Object[] array = new Object[0];
        array = set.toArray(array);
        System.out.println(Arrays.toString(array));
    }

}
