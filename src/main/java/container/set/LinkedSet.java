package container.set;

import container.Displayable;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class LinkedSet implements Set, Displayable {

    private int size = 0;
    private Item first, last;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(@NotNull Object o) {
        return getItem(o) != null;
    }

    /**
     * Returns Item with chosen object or null if there is no such Item.
     */
    private Item getItem(@NotNull Object object) {
        Item current = first;
        for (int i = 0; i < size; i++) {
            if (current.value.equals(object)) {
                return current;
            } else {
                current = current.next;
            }
        }

        return null;
    }

    @Override
    @NotNull
    public Iterator iterator() {
        return new Iterator<Object>() {
            private int index = -1;
            private Item current = first;

            @Override
            public boolean hasNext() {
                return index < size - 1;
            }

            @Override
            public Object next() {
                if (index == -1) {
                    return current.value;
                } else {
                    current = current.next;
                    return current.value;
                }
            }
        };
    }

    @Override
    @NotNull
    public Object[] toArray() {
        return toArray(new Object[size]);
    }

    @Override
    public boolean add(Object o) {
        boolean contains = contains(o);
        if (!contains) size++;
        if (size == 1) {
            first = last = new Item(o);
            return true;
        } else if (!contains) {
            last.next = new Item(o);
            last.next.prev = last;
            last = last.next;
            return true;
        } else return false;
    }

    @Override
    public boolean remove(@NotNull Object o) {
        Item item = getItem(o);
        if (item == null) {
            return false;
        }

        return remove(item);
    }

    private boolean remove(@NotNull Item item) {
        if (item.prev != null) item.prev.next = item.next;
        if (item.next != null) item.next.prev = item.prev;
        size--;

        return true;
    }

    @Override
    public boolean addAll(@NotNull Collection c) {
        boolean modified = false;
        for (Object element : c) {
            if (add(element)) modified = true;
        }

        return modified;
    }

    @Override
    public void clear() {
        first = last = null;
        size = 0;
    }

    @Override
    public boolean removeAll(@NotNull Collection c) {
        boolean modified = true;
        for (Object element : c) {
            if (!remove(element)) modified = false;
        }

        return modified;
    }

    @Override
    public boolean retainAll(@NotNull Collection c) {
        boolean modified = false;
        Item current = first;
        for (int i = 0; i < size; i++) {
            if (!c.contains(current.value)) {
                remove(current);
                modified = true;
            }
            current = current.next;
        }

        return modified;
    }

    @Override
    public boolean containsAll(@NotNull Collection c) {
        for (Object element : c) {
            if (!contains(element)) return false;
        }

        return true;
    }

    @Override
    @NotNull
    public Object[] toArray(@NotNull Object[] a) {
        int size = size();
        if (a.length < size) a = new Object[size];
        Item current = first;
        for (int i = 0; i < size; i++) {
            a[i] = current.value;
            current = current.next;
        }

        return a;
    }

    @Override
    public void display() {
        Item current = first;
        for (int i = 0; i < size; i++) {
            System.out.print(current.value + " ");
            current = current.next;
        }
        System.out.println();
    }

    private class Item {
        private Item prev;
        private Item next;
        private Object value;

        private Item(Object value) {
            this.value = value;
        }
    }

}
