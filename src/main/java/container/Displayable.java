package container;

public interface Displayable {
    void display();
}
