package container.array;

import container.model.StudentJava;

import java.util.Date;

public class StudentArrayContainerJava extends ArrayContainerJava<StudentJava> {

    public StudentArrayContainerJava(int capacity) {
        super(StudentJava.class, capacity);
    }

    @Override
    public void insert(StudentJava student) {
        if (size == -1) {
            array[++size] = student;
        } else {
            insert(searchIndexFor(student), student);
        }
    }

    private int searchIndexFor(StudentJava student) {
        Date date = student.getDateOfBirth();
        if (size == 0) {
            if (array[0].getDateOfBirth().after(date)) {
                return 0;
            } else {
                return 1;
            }
        }
        int first = 0;
        int last = size;
        int pos = (first + last) / 2;
        while (array[pos].getDateOfBirth().after(date) || array[pos + 1].getDateOfBirth().before(date)) {
            if (first == last) {
                return last + 1;
            } else if (date.after(array[pos].getDateOfBirth())) {
                last = size;
                first = pos + 1;
                pos = (first + last) / 2;
            } else if (date.before(array[pos].getDateOfBirth())) {
                last = pos;
                pos = (first + last) / 2;
            } else {
                return pos + 1;
            }
        }

        return pos + 1;
    }

    public void remove(String surname, String name, String patronymic) {
        for (int i = 0; i <= size; i++) {
            StudentJava student = array[i];
            if (student.getSurname().equals(surname)
                    && student.getName().equals(name)
                    && student.getPatronymic().equals(patronymic)) {
                remove(i);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Students:\n");
        for (int i = 0; i <= size; i++) {
            stringBuilder.append("\t").append(array[i]).append("\n");
        }

        return stringBuilder.toString();
    }

}
