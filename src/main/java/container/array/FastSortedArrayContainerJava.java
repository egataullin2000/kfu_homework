package container.array;

public class FastSortedArrayContainerJava extends SortedArrayContainerJava {

    public FastSortedArrayContainerJava(int capacity) {
        super(capacity);
    }

    @Override
    public void insert(Long value) {
        if (size == -1) {
            array[++size] = value;
        } else {
            insert(searchIndexFor(value), value);
        }
    }

    private int searchIndexFor(long value) {
        if (size == 0) {
            if (array[0] > value) {
                return 0;
            } else {
                return 1;
            }
        }
        int first = 0;
        int last = size;
        int pos = (first + last) / 2;
        while ((array[pos] >= value) || ((pos + 1 < last) && (array[pos + 1] < value))) {
            if (first == last) {
                return last + 1;
            } else if (value > array[pos]) {
                last = size;
                first = pos + 1;
                pos = (first + last) / 2;
            } else if (value < array[pos]) {
                last = pos;
                pos = (first + last) / 2;
            } else {
                return pos + 1;
            }
        }

        return pos + 1;
    }

}
