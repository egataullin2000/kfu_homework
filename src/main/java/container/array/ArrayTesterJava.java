package container.array;

import java.util.Random;

public class ArrayTesterJava {

    public static void main(String[] args) {
        test(100);
        test(1000);
        test(100000);
    }

    private static void test(int capacity) {
        System.out.println("Testing for " + capacity + " items...\n");
        SortedArrayContainerJava sortedArray = new SortedArrayContainerJava(capacity);
        FastSortedArrayContainerJava fastSortedArray = new FastSortedArrayContainerJava(capacity);
        for (int i = 0; i < capacity; i++) {
            long value = new Random().nextLong();
            System.out.print("SortedArrayContainer: \t");
            long startTime = System.nanoTime();
            sortedArray.insert(value);
            notifyItemInserted(value, startTime);
            System.out.print("FastSortedArrayContainer: ");
            startTime = System.nanoTime();
            fastSortedArray.insert(value);
            notifyItemInserted(value, startTime);
        }
        System.out.println();
    }

    private static void notifyItemInserted(long value, long startTime) {
        System.out.println("\titem " + value + " inserted, it took " + (System.nanoTime() - startTime) + " nanoseconds");
    }

}
