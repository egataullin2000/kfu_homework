package container.array;

import java.lang.reflect.Array;

public class ArrayContainerJava<E> {

    protected E[] array;
    protected int size;

    @SuppressWarnings("unchecked")
    public ArrayContainerJava(Class<E> c, int capacity) {
        array = (E[]) Array.newInstance(c, capacity);
        size = -1;
    }

    public void insert(E item) {
        array[++size] = item;
    }

    public E get(int index) {
        if (index <= size) {
            return array[index];
        } else {
            throw new IndexOutOfBoundsException("Size index is smaller than requested in get() operation");
        }
    }

    @SuppressWarnings("ManualArrayCopy")
    public void remove(int index) {
        if (index <= size) {
            for (int i = index; i < size; i++) {
                array[i] = array[i + 1];
            }
            array[size--] = null;
        } else {
            throw new IndexOutOfBoundsException("Size index is smaller than requested in remove() operation");
        }
    }

    @SuppressWarnings("ManualArrayCopy")
    protected void insert(int index, E item) {
        if (index <= array.length) {
            for (int i = ++size; i > index; i--) {
                array[i] = array[i - 1];
            }
            array[index] = item;
        } else {
            throw new IndexOutOfBoundsException("Size index is smaller than requested in insert() operation");
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <= size; i++) {
            stringBuilder.append(array[i]).append(" ");
        }
        return stringBuilder.toString();
    }

}
