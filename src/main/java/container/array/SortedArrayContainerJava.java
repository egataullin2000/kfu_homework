package container.array;

public class SortedArrayContainerJava extends ArrayContainerJava<Long> {

    public SortedArrayContainerJava(int capacity) {
        super(Long.class, capacity);
    }

    @Override
    public void insert(Long value) {
        if (size == -1) {
            array[++size] = value;
        } else {
            for (int i = 0; i <= size; i++) {
                if (array[i] > value) {
                    insert(i, value);
                    break;
                }
            }
            if (array[size] < value) {
                array[++size] = value;
            }
        }
    }

}
