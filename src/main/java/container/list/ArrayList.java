package container.list;

import container.Displayable;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.*;

public class ArrayList<E> implements List<E>, Displayable {

    private static final int DEFAULT_CAPACITY = 10;

    private Class<E> c;
    private E[] array;
    private int size;

    public ArrayList(Class<E> c) {
        this(c, DEFAULT_CAPACITY);
    }

    public ArrayList(Class<E> c, int initialCapacity) {
        this.c = c;
        initArray(c, initialCapacity);
    }

    @SuppressWarnings("unchecked")
    private void initArray(Class<E> c, int initialCapacity) {
        array = (E[]) Array.newInstance(c, initialCapacity);
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(@NotNull Object o) {
        return indexOf(o) != -1;
    }

    @Override
    @NotNull
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < size - 1;
            }

            @Override
            public E next() {
                return array[index++];
            }

            @Override
            public void remove() {
                ArrayList.this.remove(index);
            }
        };
    }


    @Override
    public boolean remove(@NotNull Object o) {
        return remove(indexOf(o)) != null;
    }

    @Override
    public void clear() {
        if (!isEmpty()) initArray(c, DEFAULT_CAPACITY);
    }

    @Override
    public E get(int index) {
        if (index > size || index < 0) throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        return array[index];
    }

    @Override
    public E set(int index, E element) {
        if (index > size || index < 0) throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        array[index] = element;
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean addAll(@NotNull Collection c) {
        for (Object collect : c) add((E) collect);

        return true;
    }


    @Override
    public boolean add(@NotNull E item) {
        if (size == array.length) increaseCapacity();
        array[size++] = item;

        return true;
    }

    @Override
    public void add(int index, @NotNull E element) {
        if (index < 0 || index > size) return;
        if (size == array.length) increaseCapacity();
        System.arraycopy(array, index, array, index + 1, size - index);
        array[index] = element;
        size++;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if (c == null) return false;
        if (index < 0 || index > size || index - size > 1) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        if (array.length - size < c.size()) increaseCapacity(c.size() - array.length + size);
        for (E element : c) add(element);

        return true;
    }

    @Override
    public E remove(int index) {
        if (index == -1) return null;
        if (index < 0 || index > size) throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        E item = array[index];
        System.arraycopy(array, index + 1, array, index, array.length - index - 1);
        array[size-- - 1] = null;

        return item;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (array[i].equals(o)) return i;
        }

        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size - 1; i >= 0; i--) {
            if (array[i].equals(o)) return i;
        }

        return -1;
    }

    @SuppressWarnings("unchecked")
    @Override
    @NotNull
    public List<E> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || fromIndex > size - 1 || toIndex < 0 || toIndex > size - 1) {
            throw new IndexOutOfBoundsException("Index: " + fromIndex + ", Size: " + toIndex);
        }
        int length = toIndex - fromIndex + 1;
        E[] array = (E[]) Array.newInstance(c, length);
        System.arraycopy(this.array, fromIndex, array, 0, length);

        return Arrays.asList(array);
    }

    @Override
    @NotNull
    public ListIterator listIterator() {
        return listIterator(0);
    }

    @Override
    @NotNull
    public ListIterator listIterator(int index) {
        return new ListIterator(index);
    }

    @SuppressWarnings("unchecked")
    @Override
    @NotNull
    public <T> T[] toArray(T[] a) {
        return Arrays.copyOf((T[]) new Object[a.length], a.length);
    }

    @Override
    @NotNull
    public Object[] toArray() {
        return Arrays.copyOf(array, this.size());
    }

    @Override
    public boolean retainAll(@NotNull Collection c) {
        if (isEmpty()) return false;
        for (int i = 0; i < size; i++) {
            if (!c.contains(array[i])) {
                remove(array[i]);
            } else i++;
        }

        return true;
    }

    @Override
    public boolean removeAll(@NotNull Collection c) {
        if (isEmpty()) return false;
        for (Object o : c) remove(o);

        return true;
    }

    @Override
    public boolean containsAll(@NotNull Collection c) {
        if (isEmpty()) return false;
        boolean containsAll = true;
        for (Object o : c) {
            if (!contains(o)) containsAll = false;
        }

        return containsAll;
    }

    private void increaseCapacity() {
        increaseCapacity(DEFAULT_CAPACITY);
    }

    private void increaseCapacity(int additionalCapacity) {
        array = Arrays.copyOf(array, array.length + additionalCapacity);
    }

    @Override
    public void display() {
        for (E e : this) {
            System.out.print(e + " ");
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArrayList<?> that = (ArrayList<?>) o;

        return size == that.size && array.length == that.array.length && Arrays.equals(array, that.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(size, array.length);
        result = 31 * result + Arrays.hashCode(array);

        return result;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        forEach(it -> stringBuilder.append(it).append(" "));

        return stringBuilder.toString();
    }

    public class ListIterator implements java.util.ListIterator<E> {
        private int index;

        private ListIterator(int index) {
            this.index = index - 1;
        }

        @Override
        public boolean hasNext() {
            return index < size - 1;
        }

        @Override
        public E next() {
            return array[++index];
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public E previous() {
            return array[--index];
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            ArrayList.this.remove(index);
        }

        @Override
        public void set(E value) {
            ArrayList.this.set(index, value);
        }

        @Override
        public void add(E item) {
            ArrayList.this.add(item);
        }
    }

}
