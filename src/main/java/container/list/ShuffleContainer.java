package container.list;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.List;

public class ShuffleContainer<E> implements Iterable<E> {

    private List<E> list;

    public ShuffleContainer(List<E> list) {
        this.list = list;
    }

    @Override
    @NotNull
    public Iterator<E> iterator() {
        return list.iterator();
    }

    public Iterator<E> shuffle() {
        return new Iterator<E>() {
            int index = list.size();

            @Override
            public boolean hasNext() {
                return index > 0;
            }

            @Override
            public E next() {
                return list.get(--index);
            }
        };
    }
}
