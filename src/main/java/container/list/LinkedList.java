package container.list;

import container.Displayable;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class LinkedList<E> implements List<E>, Displayable {

    private int size;
    private Item first;
    private Item last;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(@NotNull Object o) {
        return indexOf(o) != -1;
    }

    @Override
    @NotNull
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private Item item = first;

            @Override
            public boolean hasNext() {
                return item.next != null;
            }

            @Override
            public E next() {
                item = item.next;
                return item.value;
            }
        };
    }

    @Override
    @NotNull
    public Object[] toArray() {
        Object[] array = new Object[size];
        Item current = first;
        for (int i = 0; i < size; i++) {
            array[i] = current.value;
            current = current.next;
        }

        return array;
    }

    @Override
    public boolean add(@NotNull E item) {
        add(0, item);
        return true;
    }

    @Override
    public boolean remove(@NotNull Object o) {
        Item item = getItem(o);
        if (item == null) {
            return false;
        }

        return remove(item);
    }

    /**
     * Returns Item with chosen object.
     * Returns null if there is no such Item.
     */
    private Item getItem(@NotNull Object object) {
        Item current = first;
        for (int i = 0; i < size; i++) {
            if (current.value.equals(object)) {
                return current;
            } else {
                current = current.next;
            }
        }

        return null;
    }

    private boolean remove(@NotNull Item item) {
        if (item == first) {
            first = first.next;
        } else if (item == last) {
            last = last.prev;
        } else {
            if (item.prev != null) item.prev.next = item.next;
            if (item.next != null) item.next.prev = item.prev;
        }
        size--;

        return true;
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends E> c) {
        return addAll(0, c);
    }

    @Override
    public boolean addAll(int index, @NotNull Collection<? extends E> c) {
        int i = 0;
        for (E element : c) add(index + i++, element);

        return true;
    }

    @Override
    public void clear() {
        first = last = null;
        size = 0;
    }

    @Override
    public E get(int index) {
        return getItem(index).value;
    }

    /**
     * Returns Item with specified index or first Item if
     */
    @NotNull
    private Item getItem(int index) {
        if (index > size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
        Item current = first;
        for (int i = 0; i < index; i++) current = current.next;

        return current;
    }

    @Override
    public E set(int index, @NotNull E element) {
        Item item = getItem(index);
        E lastValue = item.value;
        item.value = element;

        return lastValue;
    }

    @Override
    public void add(int index, @NotNull E element) {
        add(index, new Item(element));
    }

    private void add(@NotNull Item item) {
        add(size, item);
    }

    private void add(int index, @NotNull Item item) {
        size++;
        if (size == 1) {
            first = last = item;
        } if (index == 0) {
            item.next = first;
            first.prev = item;
            first = item;
        } else {
            Item prev = getItem(index - 1);
            item.prev = prev;
            item.next = prev.next;
            prev.next = item;
        }
    }

    @Override
    public E remove(int index) {
        Item item = getItem(index);
        remove(item);

        return item.value;
    }

    @Override
    public int indexOf(@NotNull Object o) {
        Item current = first;
        for (int i = 0; i < size; i++) {
            if (current.value.equals(o)) {
                return i;
            } else current = current.next;
        }

        return -1;
    }

    @Override
    public int lastIndexOf(@NotNull Object o) {
        Item current = last;
        for (int i = 0; i < size; i++) {
            if (current.value.equals(o)) {
                return i;
            } else current = current.prev;
        }

        return -1;
    }

    @Override
    @NotNull
    public ListIterator listIterator() {
        return listIterator(0);
    }

    @Override
    @NotNull
    public ListIterator listIterator(int index) {
        return new ListIterator(index);
    }

    @Override
    @NotNull
    public List<E> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || fromIndex > size - 1 || toIndex < 0 || toIndex > size - 1) {
            throw new IndexOutOfBoundsException("Index: " + fromIndex + ", Size: " + toIndex);
        }
        LinkedList<E> subList = new LinkedList<>();
        Item current = first;
        for (int i = fromIndex; i < toIndex; i++) {
            subList.add(current);
            current = current.next;
        }

        return subList;
    }

    @Override
    public boolean retainAll(@NotNull Collection c) {
        if (c.isEmpty()) return false;
        Item current = first;
        for (int i = 0; i < size; i++) {
            if (!c.contains(current.value)) {
                remove(current);
            }
            current = current.next;
        }

        return true;
    }

    @Override
    public boolean removeAll(@NotNull Collection c) {
        if (c.isEmpty()) return false;
        for (Object element : c) remove(element);

        return true;
    }

    @Override
    public boolean containsAll(@NotNull Collection c) {
        for (Object element : c) {
            if (!contains(element)) return false;
        }

        return true;
    }

    @Override
    @NotNull
    public Object[] toArray(@NotNull Object[] a) {
        return new Object[0];
    }

    @Override
    public void display() {
        Item current = first;
        for (int i = 0; i < size; i++) {
            System.out.print(current.value + " ");
            current = current.next;
        }
    }

    private class Item {
        private Item prev;
        private Item next;
        private E value;

        private Item(E value) {
            this.value = value;
        }
    }

    public class ListIterator implements java.util.ListIterator<E> {
        private int index;
        private Item current = first;

        private ListIterator(int index) {
            this.index = index - 1;
        }

        @Override
        public boolean hasNext() {
            return index < size - 1;
        }

        @Override
        public E next() {
            if (++index != 0) {
                current = current.next;
            }

            return current.value;
        }

        @Override
        public boolean hasPrevious() {
            return index > -1;
        }

        @Override
        public E previous() {
            index--;
            current = current.prev;

            return current.value;
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            current = current.prev;
            LinkedList.this.remove(current.next);
        }

        @Override
        public void set(E value) {
            current.value = value;
        }

        @Override
        public void add(E item) {
            LinkedList.this.add(item);
        }
    }

}
