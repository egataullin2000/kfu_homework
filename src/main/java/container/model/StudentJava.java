package container.model;

import java.util.Date;

public class StudentJava implements Comparable<StudentJava> {

    private String surname;
    private String name;
    private String patronymic;
    private Date dateOfBirth;

    public StudentJava(String surname, String name, String patronymic) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
    }

    public StudentJava(String surname, String name, String patronymic, Date dateOfBirth) {
        this(surname, name, patronymic);
        this.dateOfBirth = dateOfBirth;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public int compareTo(StudentJava other) {
        return dateOfBirth.compareTo(other.dateOfBirth);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof StudentJava) {
            StudentJava other = (StudentJava) o;
            return other.surname.equals(surname) && other.name.equals(name) && other.patronymic.equals(patronymic);
        } else return false;
    }

    @Override
    @SuppressWarnings("StringBufferReplaceableByString")
    public String toString() {
        return new StringBuilder(surname).append(" ")
                .append(name).append(" ")
                .append(patronymic).append(", ")
                .append(dateOfBirth)
                .toString();
    }

}
