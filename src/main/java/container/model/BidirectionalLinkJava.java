package container.model;

public class BidirectionalLinkJava<E extends Comparable<E>> {

    private E value;
    private BidirectionalLinkJava<E> prevLink;
    private BidirectionalLinkJava<E> nextLink;

    public BidirectionalLinkJava(E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public boolean hasPrev() {
        return prevLink != null;
    }

    public boolean hasNext() {
        return nextLink != null;
    }

    public BidirectionalLinkJava<E> getPrevLink() {
        return prevLink;
    }

    public void setPrevLink(BidirectionalLinkJava<E> link) {
        prevLink = link;
    }

    public BidirectionalLinkJava<E> getNextLink() {
        return nextLink;
    }

    public void setNextLink(BidirectionalLinkJava<E> link) {
        nextLink = link;
    }

    @Override
    public String toString() {
        return value.toString();
    }

}
