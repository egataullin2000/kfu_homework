package container.model;

public class LinkJava<E extends Comparable<E>> {

    private E value;
    private LinkJava<E> nextLink;

    public LinkJava(E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public boolean hasNext() {
        return nextLink != null;
    }

    public LinkJava<E> getNextLink() {
        return nextLink;
    }

    public void setNextLink(LinkJava<E> link) {
        nextLink = link;
    }

    @Override
    public String toString() {
        return value.toString();
    }

}
