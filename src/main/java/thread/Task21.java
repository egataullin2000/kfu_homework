package thread;

public class Task21 extends Thread {

    public static void main(String[] args) {
        Task21 thread1 = new Task21();
        Task21 thread2 = new Task21();
        Task21 thread3 = new Task21();
        Task21 thread4 = new Task21();
        Task21 thread5 = new Task21();

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
    }

    @Override
    public void run() {
        long startTime = System.nanoTime();
        Fibonacci.get(11);
        String msg = "The executionTime of thread " + currentThread().getName() + " is " + (System.nanoTime() - startTime);
        System.out.println(msg);
    }

}
