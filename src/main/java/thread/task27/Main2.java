package thread.task27;

public class Main2 implements Runnable {

    IntegerGenerator integerGenerator;
    private Object syncPoint;

    Main2(IntegerGenerator integerGenerator) {
        this.integerGenerator = integerGenerator;
    }

    public void setSyncPoint(Object syncPoint) {
        this.syncPoint = syncPoint;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            i++;
            int value = integerGenerator.getNext(syncPoint);
            if (value % 2 != 0) {
                System.out.println(value + " " + value % 2);
                System.exit(-1);
            }
            if (i % 1000 == 0) {
                System.out.println(value);
            }
        }
    }

    public static void main(String[] args) {
        IntegerGenerator integerGenerator = new SynchronizedIntegerGenerator();
        Main2 main1 = new Main2(integerGenerator);
        Main2 main2 = new Main2(integerGenerator);
        Main2 main3 = new Main2(integerGenerator);
        Main2 main4 = new Main2(integerGenerator);
        Object sync1 = new Object();
        Object sync2 = new Object();
        main1.setSyncPoint(sync1);
        main2.setSyncPoint(sync1);
        main3.setSyncPoint(sync2);
        main4.setSyncPoint(sync2);
        Thread thread1 = new Thread(main1);
        Thread thread2 = new Thread(main2);
        Thread thread3 = new Thread(main3);
        Thread thread4 = new Thread(main4);
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }
}