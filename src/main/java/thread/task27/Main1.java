package thread.task27;

public class Main1 implements Runnable {

    private static Main1[] mains;
    private IntegerGenerator integerGenerator;
    private Object syncPoint;

    private Main1(IntegerGenerator integerGenerator) {
        this.integerGenerator = integerGenerator;
    }

    private void setSyncPoint(Object syncPoint) {
        this.syncPoint = syncPoint;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            i++;
            int value = integerGenerator.getNext(syncPoint);
            if (value % 2 != 0) {
                System.out.println(value + " " + value % 2);
                System.exit(-1);
            }
            if (i % 1000 == 0) {
                System.out.println(value);
            }
        }
    }

    public static void main(String[] args) {
        mains = getMainArray();
        setSyncPoints(new Object());
        startThreads(getThreads());
    }

    private static Main1[] getMainArray() {
        Main1[] mains = new Main1[4];
        IntegerGenerator generator = new SynchronizedIntegerGenerator();
        for (int i = 0; i < mains.length; i++) {
            mains[i] = new Main1(generator);
        }

        return mains;
    }

    private static void setSyncPoints(Object syncPoint) {
        for (Main1 main : mains) {
            main.setSyncPoint(syncPoint);
        }
    }

    private static Thread[] getThreads() {
        Thread[] threads = new Thread[4];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(mains[i]);
        }

        return threads;
    }

    private static void startThreads(Thread[] threads) {
        for (Thread thread : threads) {
            thread.start();
        }
    }
}