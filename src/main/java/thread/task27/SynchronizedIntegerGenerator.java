package thread.task27;

public class SynchronizedIntegerGenerator implements IntegerGenerator {
    private int value = 0;

    @Override
    public int getNext(Object syncPoint) {
        synchronized (syncPoint) {
            value++;
            value++;
            return value;
        }
    }
}