package thread.task27;

public interface IntegerGenerator {
    int getNext(Object o);
}