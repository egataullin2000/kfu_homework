package thread;

import java.util.stream.Stream;

public class Task23 {

    public static void main(String[] args) throws Exception {
        System.out.println("Max priority, daemon and yielding threads: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    true,
                    10,
                    true,
                    "Max priority, daemon and yielding thread"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        System.out.println("Min priority, daemon and yielding threads: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    true,
                    1,
                    true,
                    "Min priority, daemon and yielding thread"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        System.out.println("Max priority, yielding not daemon threads: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    true,
                    10,
                    false,
                    "Max priority, yielding not daemon thread"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        System.out.println("Min priority, yielding not daemon threads: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    true,
                    1,
                    false,
                    "Min priority, yielding not daemon thread"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        System.out.println("Max priority, daemon not yielding threads: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    false,
                    10,
                    true,
                    "Max priority daemon thread without yielding"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        System.out.println("Min priority, daemon threads without yielding: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    false,
                    1,
                    true,
                    "Min priority daemon thread without yielding"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        System.out.println("Max priority, not daemon threads without yielding: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    false,
                    10,
                    false,
                    "Max priority not daemon thread without yielding"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());

        System.out.println("Min priority, not daemon threads without yielding: " + Stream.generate(() -> {
            FibonacciThread[] threads = initThreads(
                    false,
                    1,
                    false,
                    "Min priority not daemon thread without yielding"
            );
            long startTime = System.nanoTime();
            try {
                runThreads(threads);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return System.nanoTime() - startTime;
        }).limit(100).mapToLong(e -> e).summaryStatistics());
    }

    private static FibonacciThread[] initThreads(boolean withYield, int priority, boolean areDaemons, String name) {
        FibonacciThread[] threads = new FibonacciThread[5];
        for (int i = 0; i < 5; i++) {
            if (withYield) {
                threads[i] = new FibThreadWithYield();
            } else threads[i] = new FibThreadWithoutYield();
            threads[i].setPriority(priority);
            threads[i].setDaemon(areDaemons);
            threads[i].setName(name);
        }

        return threads;
    }

    private static void runThreads(FibonacciThread[] threads) throws InterruptedException {
        for (FibonacciThread thread : threads) {
            thread.start();
        }
        for (FibonacciThread thread : threads) {
            thread.join();
        }
    }

    private static abstract class FibonacciThread extends Thread {
        long executionTime;

        public long getExecutionTime() {
            return executionTime;
        }

        abstract void benchmark(int n);
    }

    private static class FibThreadWithYield extends FibonacciThread {
        @Override
        public void run() {
            benchmark(10);
            Thread.yield();
        }

        @Override
        public void benchmark(int n) {
            long startTime = System.nanoTime();
            Fibonacci.get(n);
            executionTime = System.nanoTime() - startTime;
        }
    }

    private static class FibThreadWithoutYield extends FibonacciThread {
        @Override
        public void run() {
            benchmark(10);
        }

        @Override
        public void benchmark(int n) {
            long start = System.nanoTime();
            Fibonacci.get(n);
            executionTime = System.nanoTime() - start;
        }
    }

}