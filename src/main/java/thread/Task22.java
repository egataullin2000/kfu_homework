package thread;

public class Task22 implements Runnable {

    public static void main(String[] args) throws InterruptedException {
        runNewThread(10);
        runNewThread(7);
        runNewThread(5);
        runNewThread(3);
        runNewThread(1);
    }

    private static void runNewThread(int priority) throws InterruptedException {
        Thread thread = new Thread(new Task22());
        thread.setPriority(priority);
        long startTime = System.nanoTime();
        thread.start();
        thread.join();
        notifyThreadFinished(priority, System.nanoTime() - startTime);
    }

    private static void notifyThreadFinished(int priority, long time) {
        System.out.println("Thread with priority " + priority + " finished in " + time + " ns");
    }

    @Override
    public void run() {
        Fibonacci.get(11);
    }

}
