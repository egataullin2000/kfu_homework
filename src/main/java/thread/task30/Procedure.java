package thread.task30;

@FunctionalInterface
public interface Procedure {
    void invoke();
}