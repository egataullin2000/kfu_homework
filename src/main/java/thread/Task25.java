package thread;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

public class Task25 implements Callable<Map<Integer, Long>> {

    private int n;

    public static void main(String[] args) throws InterruptedException {
        run(Executors.newSingleThreadExecutor(), "SingleThreadExecutor");
        run(Executors.newFixedThreadPool(4), "FixedThreadPoolExecutor");
        run(Executors.newCachedThreadPool(), "CachedThreadPoolExecutor");
    }

    private static void run(ExecutorService executor, String executorName) throws InterruptedException {
        long startTime = System.nanoTime();
        for (int i = 0; i < 10; i++) {
            Future<Map<Integer, Long>> future = executor.submit(new Task25().get(17));
        }
        executor.shutdown();
        executor.awaitTermination(Thread.MAX_PRIORITY, TimeUnit.HOURS);
        System.out.println(executorName + " finished in " + (System.nanoTime() - startTime) + " ns");
    }

    private Task25 get(int n) {
        this.n = n;
        return this;
    }

    @Override
    public Map<Integer, Long> call() {
        Map<Integer, Long> map = new HashMap<>();
        long startTime = System.nanoTime();
        int num = Fibonacci.get(n);
        map.put(num, System.nanoTime() - startTime);

        return map;
    }

}
