package thread;

public class Fibonacci {
    public static int get(int number) {
        int result;
        if (number == 0) {
            return 0;
        } else if (number == 1) {
            return 1;
        }
        result = get(number - 1) + get(number - 2);
        System.out.println(Thread.currentThread().getName() + ": " + result);

        return result;
    }
}
