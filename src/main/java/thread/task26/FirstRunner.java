package thread.task26;

public class FirstRunner implements Runnable {
    private Changeable second = new Runner2();
    private Changeable third = new Runner3();

    @Override
    public void run() {
        new Thread(((Runnable) second)).start();
        long time1 = System.nanoTime();
        while (second.getState()) {
            Thread.yield();
        }
        time1 = System.nanoTime() - time1;

        new Thread(((Runnable) third)).start();
        long time2 = System.nanoTime();
        while (third.getState()) {
            Thread.yield();
        }
        time2 = System.nanoTime() - time2;

        Main.times1.add(time1);
        Main.times2.add(time2);
    }
}