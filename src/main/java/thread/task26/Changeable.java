package thread.task26;

public interface Changeable {
    boolean getState();
}
