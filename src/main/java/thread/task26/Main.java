package thread.task26;

import java.util.ArrayList;

public class Main {
    static ArrayList<Long> times1 = new ArrayList<>();
    static ArrayList<Long> times2 = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            Thread thread = new Thread(new FirstRunner());

            thread.start();
            thread.join();
        }

        System.out.println("First statistics: " + times1.stream().mapToLong(e -> e).summaryStatistics());
        System.out.println("Second statistics: " + times2.stream().mapToLong(e -> e).summaryStatistics());
    }
}