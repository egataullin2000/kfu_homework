package thread.task26;

public class Runner2 implements Runnable, Changeable {
    private boolean someState = true;

    @Override
    public boolean getState() {
        return someState;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10);
            someState = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}