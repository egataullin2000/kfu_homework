package thread;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

public class Task28 {
    static class FibThread implements Runnable {
        @Override
        public void run() {
            //long start = System.nanoTime();
            nthFib(10);
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println(Thread.currentThread().getName() + " finished calculating in: " + (System.nanoTime() - start) / 1_000_000 + " ms.");
        }

        private int nthFib(int n) {
            try {

                if (n == 0 || n == 1) {
                    writer.write(1);
                    return 1;
                } else {
                    int res = nthFib(n - 2) + nthFib(n - 1);
                    writer.write(res);
                    return res;
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
    }

    static class FibReader implements Runnable {
        @Override
        public void run() {
            try {
                while (reader.ready()) {
                    System.out.println(reader.read());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static PipedReader reader = new PipedReader();
    static PipedWriter writer = new PipedWriter();

    public static void main(String[] args) throws IOException {
        reader.connect(writer);
        new Thread(new FibThread()).start();
        new Thread(new FibReader()).start();
    }
}