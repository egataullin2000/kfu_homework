package thread.task29.no_deadlock;

public class Policeman implements Runnable {
    @Override
    public void run() {
        synchronized (Place.accomplice) {
            System.out.println("Полицейский арестовал сообщника");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Полицейский отпускает сообщника, чтобы спасти девушку");
        }
    }
}