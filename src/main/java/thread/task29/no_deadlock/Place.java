package thread.task29.no_deadlock;

import thread.task29.model.Accomplice;
import thread.task29.model.Girl;

public class Place {
    static Accomplice accomplice = new Accomplice();
    static Girl girl = new Girl();

    public static void main(String[] args) {
        new Thread(new Policeman()).start();
        new Thread(new Attempt()).start();
    }
}