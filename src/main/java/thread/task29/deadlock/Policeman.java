package thread.task29.deadlock;

public class Policeman implements Runnable {
    @Override
    public void run() {
        synchronized (Place.accomplice) {
            System.out.println("Полицейский арестовал сообщника");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Полицейский ждет, когда преступник отпустит девушку, иначе он не отпустит его сообщника");
            synchronized (Place.girl) {
                System.out.println("Девушка отпущена");
            }
        }
    }
}