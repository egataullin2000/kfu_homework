package thread.task29.deadlock;

public class Attempt implements Runnable {
    @Override
    public void run() {
        synchronized (Place.girl) {
            System.out.println("Преступник схватил девушку");
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Преступник ждёт, когда полицейский отпустит его сообщника, иначе он не отпустит девушку");
            synchronized (Place.accomplice) {
                System.out.println("Сообщник отпущен");
            }
        }
    }
}