package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Task24 implements Runnable {

    public static void main(String[] args) throws InterruptedException {
        run(Executors.newSingleThreadExecutor(), "SingleThreadExecutor");
        run(Executors.newFixedThreadPool(4), "FixedThreadPoolExecutor");
        run(Executors.newCachedThreadPool(), "CachedThreadPoolExecutor");
    }

    private static void run(ExecutorService executor, String executorName) throws InterruptedException {
        long startTime = System.nanoTime();
        for (int i = 0; i < 10; i++) {
            executor.submit(new Task24());
        }
        executor.shutdown();
        executor.awaitTermination(Thread.MAX_PRIORITY, TimeUnit.HOURS);
        System.out.println(executorName + " finished in " + (System.nanoTime() - startTime) + " ns");
    }

    @Override
    public void run() {
        Fibonacci.get(17);
    }

}
