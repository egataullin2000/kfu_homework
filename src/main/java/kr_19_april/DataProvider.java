package kr_19_april;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class DataProvider {

    private static final String SOURCE = "data.txt";

    private static DataProvider instance;

    private Scanner scanner;

    private DataProvider() {
        try {
            scanner = new Scanner(new FileReader(SOURCE));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DataProvider get() {
        if (instance == null) instance = new DataProvider();

        return instance;
    }

    public double[] getData() {
        double[] data = new double[256];
        for (int i = 0; i < data.length; i++) {
            if (!scanner.hasNext()) break;
            data[i] = scanner.nextDouble();
        }

        return data;
    }

    public void close() {
        scanner.close();
    }

}
