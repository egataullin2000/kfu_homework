package kr_19_april;

public class Main {
    public static void main(String[] args) {
        DataProvider dataProvider = DataProvider.get();
        double[] data = dataProvider.getData();
        dataProvider.close();
        Complex[] x = new Complex[data.length];

        for (int i = 0; i < data.length; i++) {
            x[i] = new Complex(data[i], 0);
        }
        show(x, "x");

        Complex[] y = FFT.fft(x);
        show(y, "y = fft(x)");

        Complex[] z = FFT.ifft(y);
        show(z, "z = ifft(y)");

        Complex[] c = FFT.cconvolve(x, x);
        show(c, "c = cconvolve(x, x)");

        Complex[] d = FFT.convolve(x, x);
        show(d, "d = convolve(x, x)");
    }

    private static void show(Complex[] x, String title) {
        System.out.println(title);
        System.out.println("-------------------");
        for (Complex complex : x) System.out.println(complex);
        System.out.println();
    }
}