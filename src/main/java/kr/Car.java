package kr;

import java.util.Optional;
 
public class Car<T> {
    Person owner;
    Optional<InsurancePolicy> insurancePolicy;
 
    public Car() {
    }
 
    public Person getOwner() {
        return owner;
    }
 
    public void setOwner(Person owner) {
        this.owner = owner;
    }
 
    public Optional<InsurancePolicy> getInsurancePolicy() {
        return insurancePolicy;
    }
 
    public void setInsurancePolicy(Optional<InsurancePolicy> insurancePolicy) {
        this.insurancePolicy = insurancePolicy;
    }
 
    @Override
    public String toString() {
        return "Car{" +
                "owner=" + owner +
                ", insurancePolicy=" + insurancePolicy +
                '}';
    }
}
