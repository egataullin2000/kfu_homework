package kr;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        testPoint1();
        testPoint2();
        testPoint3();
        testPoint4();
        testPoint5(100);
    }

    public static void testPoint1() {
        Person person1 = new Person("Ruslan", "Futbol", 18);
        Person person2 = new Person("Edgar", "Edgarov", 17);
        Person person3 = new Person("Serega", "Seregov", 19);
        Person person4 = new Person("Radimir", "Radimirov", 25);
        Person person5 = new Person("Michael", "Michaelov", 30);
        Person person6 = new Person("Artem", "Artemov", 18);
        Person person7 = new Person("Ruslan", "Futbol", 18);
        Person person8 = new Person("Kamil", "Kamilev", 19);
        Person person9 = new Person("Nastya", "Natyava", 18);
        Person person10 = new Person("Dasha", "Dashova", 17);
 
        long startTime = System.nanoTime();
        List<Person> list1 = new ArrayList<>(
                Arrays.asList(person1, person2, person3, person4, person5, person6, person7, person8, person9, person10)
        );
        System.out.println("Time - " + (System.nanoTime() - startTime));
        startTime = System.nanoTime();
        List<Person> list2 = new LinkedList<>(list1);
        System.out.println("Time - " + (System.nanoTime() - startTime));
        startTime = System.nanoTime();
        Set<Person> set1 = new HashSet<>(list1);
        System.out.println("Time - " + (System.nanoTime() - startTime));
        startTime = System.nanoTime();
        Set<Person> set2 = new TreeSet<>(list1);
        System.out.println("Time - " + (System.nanoTime() - startTime));
 
        startTime = System.nanoTime();
        for (Person person : list1) {
            System.out.println(person);
        }
        System.out.println("Time - " + (System.nanoTime() - startTime));
 
        startTime = System.nanoTime();
        for (Person person : list2) {
            System.out.println(person);
        }
        System.out.println("Time - " + (System.nanoTime() - startTime));
 
        startTime = System.nanoTime();
        for (Person person : set1) {
            System.out.println(person);
        }
        System.out.println("Time - " + (System.nanoTime() - startTime));
 
        startTime = System.nanoTime();
        for (Person person : set2) {
            System.out.println(person);
        }
        System.out.println("Time - " + (System.nanoTime() - startTime));
    }

    
    public static void testPoint2(){
        Person person1 = new Person("Ruslan", "Futbol", 18);
        Person person2 = new Person("Edgar", "Edgarov", 17);
        Person person3 = new Person("Serega", "Seregov", 19);
        Person person4 = new Person("Radimir", "Radimirov", 25);
        Person person5 = new Person("Michael", "Michaelov", 30);
        Person person6 = new Person("Artem", "Artemov", 18);
        Person person7 = new Person("Ruslan", "Futbol", 18);
        Person person8 = new Person("Kamil", "Kamilev", 19);
        Person person9 = new Person("Nastya", "Natyava", 18);
        Person person10 = new Person("Dasha", "Dashova", 17);
 
        InsurancePolicy<Person> policy1 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy2 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy3 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy4 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy5 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy6 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy7 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy8 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy9 = new InsurancePolicy<>();
        InsurancePolicy<Person> policy10 = new InsurancePolicy<>();
 
        List<InsurancePolicy<Person>> policies = new ArrayList<>(
                Arrays.asList(policy1, policy2, policy3, policy4, policy5, policy6, policy7, policy8, policy9, policy10)
        );
        policies.get(0).setOwner(Arrays.asList(person1));
        policies.get(1).setOwner(Arrays.asList(person2));
        policies.get(2).setOwner(Arrays.asList(person3));
        policies.get(3).setOwner(Arrays.asList(person4));
        policies.get(4).setOwner(Arrays.asList(person5));
        policies.get(5).setOwner(Arrays.asList(person6));
        policies.get(6).setOwner(Arrays.asList(person7));
        policies.get(7).setOwner(Arrays.asList(person8));
        policies.get(8).setOwner(Arrays.asList(person9));
        policies.get(9).setOwner(Arrays.asList(person10));
        policies.forEach(it -> System.out.print(it + " "));
    }

    public static void testPoint3() {
        Person person1 = new Person("Ruslan", "Futbol", 18);
        Person person2 = new Person("Edgar", "Edgarov", 17);
        Person person3 = new Person("Serega", "Seregov", 19);
        Person person4 = new Person("Radimir", "Radimirov", 25);
        Person person5 = new Person("Michael", "Michaelov", 30);
        Person person6 = new Person("Artem", "Artemov", 18);
        Person person7 = new Person("Ruslan", "Futbol", 18);
        Person person8 = new Person("Kamil", "Kamilev", 19);
        Person person9 = new Person("Nastya", "Natyava", 18);
        Person person10 = new Person("Dasha", "Dashova", 17);
       
        List<Person> persons = new ArrayList<>(
                Arrays.asList(person1, person2, person3, person4, person5, person6, person7, person8, person9, person10)
        );
        persons.stream().map(it -> {
            Car<Person> car = new Car<>();
            car.setOwner(it);
            return car;
        }).peek(it -> {
            InsurancePolicy<Person> policy = new InsurancePolicy<>();
            policy.setInsurancePolicyNumber("id" + new Random().nextInt());
            policy.setOwner(Arrays.asList(it.getOwner()));
            it.setInsurancePolicy(Optional.of(policy));
        }).collect(Collectors.toList()).forEach(System.out::println);
    }

    public static void testPoint4(){
        Person person1 = new Person("Ruslan", "Futbol", 18);
        Person person2 = new Person("Edgar", "Edgarov", 17);
        Person person3 = new Person("Serega", "Seregov", 19);
        Person person4 = new Person("Radimir", "Radimirov", 25);
        Person person5 = new Person("Michael", "Michaelov", 30);
        Person person6 = new Person("Artem", "Artemov", 18);
        Person person7 = new Person("Ruslan", "Futbol", 18);
        Person person8 = new Person("Kamil", "Kamilev", 19);
        Person person9 = new Person("Nastya", "Natyava", 18);
        Person person10 = new Person("Dasha", "Dashova", 17);
 
        List<Person> persons = new ArrayList<>(
                Arrays.asList(person1, person2, person3, person4, person5, person6, person7, person8, person9, person10)
        );
        persons.stream().map(it -> {
            Car<Person> car = new Car<>();
            car.setOwner(it);
            return car;
        }).peek(it -> {
            if (new Random().nextBoolean()) {
                InsurancePolicy<Person> policy = new InsurancePolicy<>();
                policy.setInsurancePolicyNumber("id" + new Random().nextInt());
                policy.setOwner(Arrays.asList(it.getOwner()));
                it.setInsurancePolicy(Optional.of(policy));
            } else {
                it.setInsurancePolicy(Optional.empty());
            }
        }).filter(it -> it.getInsurancePolicy().isPresent()).collect(
                Collectors.groupingBy(Car::getOwner)
        ).forEach((key, value) -> System.out.println(key + " -> " + value));
    }

    public static void testPoint5(int m) {
        List<Integer> list = new ArrayList<>(m);
        Stream.iterate(new int[]{1, 1}, n -> new int[]{n[1], n[0] + n[1]})
                .limit(m)
                .map(n -> n[0])
                .forEach(x -> list.add(x));
 
        IntSummaryStatistics stats = list.stream()
                .collect(Collectors.summarizingInt(n -> n));
 
        for (int i : list) {
            System.out.println(i);
        }
        System.out.println("");
        System.out.println(stats);
    }
}
