package kr;

public class Person implements Comparable<Person> { 

    private String name; 
    private String lastName; 
    private int age; 

    public Person(String name, String lastName, int age) { 
        this.name = name; 
        this.lastName = lastName; 
        this.age = age; 
    } 

    public String getName() { 
        return name; 
    } 

    public void setName(String name) { 
        this.name = name; 
    } 

    public String getLastName() { 
        return lastName; 
    } 

    public void setLastName(String lastName) { 
        this.lastName = lastName; 
    } 

    public int getAge() { 
        return age; 
    } 

    public void setAge(int age) { 
        this.age = age; 
    } 

    @Override 
    public int compareTo(Person person) { 
        return age - person.age; 
    } 

    @Override 
    public boolean equals(Object o) { 
        if (this == o) {
            return true;
        } else if (o == null || getClass() != o.getClass()) {
            return false; 
        }

        Person person = (Person) o; 
        return name.equals(person.name) && lastName.equals(person.lastName) && (age == person.age);
    } 

    @Override 
    public String toString() { 
        return "Person{" + 
        "name='" + name + '\'' + 
        ", lastName='" + lastName + '\'' + 
        ", age=" + age + 
        '}'; 
    } 
}
