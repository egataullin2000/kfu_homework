package file;

import stream_api.model.Group;
import stream_api.model.Sex;
import stream_api.model.StudentInGroup;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

/**
 * Class work on 10 April 2019
 */
public class CW1004 {

    private static final String FILE_NAME = "students.txt";

    public static void main(String[] args) {
        List<StudentInGroup> students = new ArrayList<>(3);
        students.add(new StudentInGroup("Edgar", Optional.of(Group.GROUP_5), 19, Sex.MALE));
        students.add(new StudentInGroup("Marat", Optional.of(Group.GROUP_5), 19, Sex.MALE));
        students.add(new StudentInGroup("Mikhail", Optional.of(Group.GROUP_5), 19, Sex.MALE));

        DataProvider.writeStudents(FILE_NAME, students);

        students = DataProvider.readStudents(FILE_NAME);
        for (StudentInGroup student : students) {
            System.out.println(student);
        }
    }

    private static class DataProvider {
        private static void writeStudents(String fileName, List<StudentInGroup> students) {
            try (FileWriter writer = new FileWriter(fileName)) {
                for (StudentInGroup student : students) {
                    writer.write(
                            student.getName() + " " +
                                    student.getGroup().orElse(Group.NONE) + " " +
                                    student.getAge() + " " +
                                    student.getSex() + "\n"
                    );
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private static List<StudentInGroup> readStudents(String fileName) {
            List<StudentInGroup> students = null;
            try (Scanner scanner = new Scanner(new FileReader(fileName))) {
                students = new ArrayList<>();
                while (scanner.hasNext()) {
                    students.add(new StudentInGroup(
                            scanner.next(),
                            Optional.of(Group.valueOf(scanner.next())),
                            scanner.nextInt(),
                            Sex.valueOf(scanner.next())
                    ));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return students;
        }
    }

}
