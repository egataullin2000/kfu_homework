package file.cw1704;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DataProvider {

    private String fileName;
    private boolean wasFileAlreadyCreated;

    public DataProvider(String fileName) {
        this.fileName = fileName;
        try {
            File file = new File(fileName);
            wasFileAlreadyCreated = file.exists();
            if (!wasFileAlreadyCreated) file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Scanner getReader() {
        Scanner reader = null;
        try {
            reader = new Scanner(new FileInputStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return reader;
    }

    private FileWriter getWriter() {
        FileWriter writer = null;
        try {
            writer = new FileWriter(fileName, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return writer;
    }

    public void write(String msg) {
        try (FileWriter writer = getWriter()) {
            writer.write(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String read() {
        String msg = null;
        int oldMsgCount = 0;
        Scanner reader = getReader();
        while (reader.hasNextLine()) {
            reader.nextLine();
            oldMsgCount++;
        }
        reader.close();
        int newMsgCount = oldMsgCount;
        while (oldMsgCount == newMsgCount) {
            newMsgCount = 0;
            reader = getReader();
            while (reader.hasNextLine()) {
                msg = reader.nextLine();
                newMsgCount++;
            }
            reader.close();
        }

        return msg;
    }

    public boolean wasFileAlreadyCreated() {
        return wasFileAlreadyCreated;
    }

}
