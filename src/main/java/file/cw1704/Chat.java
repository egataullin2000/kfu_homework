package file.cw1704;

import java.util.Scanner;

public class Chat {

    private static final String MSG_TO_EXIT = "BYE";
    private static final String MSG_WAITING = "Waiting for interlocutor.";
    private static final String MSG_YOU_CAN_WRITE = "Now you can write messages!";

    private DataProvider dataProvider;

    public Chat(String fileName) {
        dataProvider = new DataProvider(fileName);
    }

    public void join(String nickname) {
        System.out.println("Enter " + MSG_TO_EXIT + " to exit the chat.");
        dataProvider.write(nickname + " joined the chat.\n");
        boolean isFirst = !dataProvider.wasFileAlreadyCreated();
        Scanner scanner = new Scanner(System.in);
        if (isFirst) {
            System.out.println(MSG_WAITING);
            System.out.println(dataProvider.read());
        }
        String msg = "";
        if (isFirst) {
            System.out.println(MSG_YOU_CAN_WRITE);
            msg = scanner.nextLine();
        } else {
            System.out.println(MSG_WAITING);
        }
        while (!msg.equals(MSG_TO_EXIT)) {
            if (!msg.equals("")) {
                dataProvider.write(msg);
                dataProvider.write("\n");
            }
            msg = dataProvider.read();
            System.out.println(msg);
            if (msg.equals(MSG_TO_EXIT)) break;
            msg = scanner.nextLine();
        }
    }

}
