package file.cw1704;

import java.util.Scanner;

public class Main {

    private static final String MSG_ENTER_FILE_NAME = "Please, enter file name";
    private static final String MSG_ENTER_NICKNAME = "Please, enter your nickname";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(MSG_ENTER_FILE_NAME);
        Chat chat = new Chat(scanner.next());
        System.out.println(MSG_ENTER_NICKNAME);
        chat.join(scanner.next());
        scanner.close();
    }

}
