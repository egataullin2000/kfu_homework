package file.cw1504;

import java.util.Scanner;

public class Main {

    private static final String MSG_READY = "Ready";

    @SuppressWarnings("StatementWithEmptyBody")
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        System.out.println("Enter file name");
        var game = new Game(scanner, scanner.next());
        System.out.println("Enter \"" + MSG_READY + "\" when you ready to start");
        while (!scanner.next().equals(MSG_READY));
        game.start();
    }

}
