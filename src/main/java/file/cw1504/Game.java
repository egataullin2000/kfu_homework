package file.cw1504;

import java.util.Scanner;

@SuppressWarnings("WeakerAccess")
public class Game {

    private static final String MSG_YOU_ARE = "You are playing as ";
    private static final String MSG_YOUR_TURN = "Now is your turn!";
    private static final String MSG_OPPONENTS_TURN = "Expecting for opponent's turn...";
    private static final String MSG_ERROR = "Error: this cell is already taken. Please, try another.";
    private static final String MSG_GAME_OVER = "Game over!";
    private static final String MSG_WINNER_IS = "The winner is ";

    private Scanner scanner;
    private GamedataProvider gamedataProvider;
    private char player;

    public Game(Scanner scanner, String fileName) {
        this.scanner = scanner;
        gamedataProvider = GamedataProvider.get(fileName);
        player = gamedataProvider.getPlayer();
    }

    public void start() {
        System.out.println("The game is started!");
        System.out.println(MSG_YOU_ARE + player);
        do {
            waitForOpponentIfNeeded();
            printPlayingField();
            if (gamedataProvider.isGameOver()) break;
            System.out.println(MSG_YOUR_TURN);
            System.out.println("Where to place " + player + "?");
            int code = gamedataProvider.tryMakeTurn(scanner.nextInt());
            while (code == GamedataProvider.ERROR) {
                System.out.println(MSG_ERROR);
                code = gamedataProvider.tryMakeTurn(scanner.nextInt());
            }
            printPlayingField();
        } while (!gamedataProvider.isGameOver());
        System.out.println(MSG_GAME_OVER);
        System.out.println(MSG_WINNER_IS + gamedataProvider.getWinner());
    }

    @SuppressWarnings("StatementWithEmptyBody")
    private void waitForOpponentIfNeeded() {
        if (gamedataProvider.isOpponentsTurn()) {
            System.out.println(MSG_OPPONENTS_TURN);
            while (gamedataProvider.isOpponentsTurn());
        }
    }

    private void printPlayingField() {
        for (char[] line : gamedataProvider.getPlayingField()) {
            for (char cell : line) {
                System.out.print(cell + " ");
            }
            System.out.println();
        }
    }

}
