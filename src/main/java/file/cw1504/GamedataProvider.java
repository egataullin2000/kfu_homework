package file.cw1504;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;

class GamedataProvider {

    private static final char PLAYER_X = 'x';
    private static final char PLAYER_0 = '0';
    static final int OK = 0;
    static final int ERROR = 1;

    private static GamedataProvider instance;

    private char player;
    private FileLock lock;
    private String fileName;
    private RandomAccessFile dataFile;

    static GamedataProvider get(String fileName) {
        if (instance == null) instance = new GamedataProvider(fileName);

        return instance;
    }

    private GamedataProvider(String fileName) {
        try {
            this.fileName = fileName;
            dataFile = new RandomAccessFile(fileName, "rw");
            if (dataFile.length() == 0) {
                player = PLAYER_X;
                setPlayerTurn(player);
                for (int i = 1; i <= 9; i++) {
                    dataFile.writeChar(48 + i);
                }
            } else player = PLAYER_0;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    char getPlayer() {
        return player;
    }

    boolean isOpponentsTurn() {
        boolean isOpponentsTurn = true;
        try {
            dataFile.seek(0);
            isOpponentsTurn = dataFile.readChar() != player;
            if (!isOpponentsTurn) tryLock();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return isOpponentsTurn;
    }

    private void setPlayerTurn(char player) {
        try {
            dataFile.seek(0);
            dataFile.writeChar(player);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void tryLock() {
        if (lock == null || !lock.isValid()) lock();
    }

    private void lock() {
        try {
            lock = dataFile.getChannel().lock(0, 1, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return 0 if OK or 1 if cell is already taken.
     */
    int tryMakeTurn(int where) {
        try {
            dataFile.seek(where * 2);
            char cell = dataFile.readChar();
            if (cell == PLAYER_X || cell == PLAYER_0) return ERROR;
            dataFile.writeChar(player);
            endTurn();
            return OK;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ERROR;
    }

    private void endTurn() {
        try {
            lock.release();
            char whosTurn;
            if (player == PLAYER_X) {
                whosTurn = PLAYER_0;
            } else whosTurn = PLAYER_X;
            setPlayerTurn(whosTurn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean isGameOver() {
        try {
            dataFile.seek(2);
            var playingField = getPlayingField();

            for (int i = 0; i < 3; i++) {
                if (playingField[i][0] == playingField[i][1] && playingField[i][0] == playingField[i][2]) return true;
            }

            for (int i = 0; i < 3; i++) {
                if (playingField[0][i] == playingField[1][i] && playingField[0][i] == playingField[2][i]) return true;
            }

            if (playingField[0][0] == playingField[1][1] && playingField[0][0] == playingField[2][2]) return true;

            return playingField[0][2] == playingField[1][1] && playingField[0][2] == playingField[2][0];
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    char[][] getPlayingField() {
        try {
            char[][] field = new char[3][3];
            for (int i = 0; i < field.length; i++) {
                for (int j = 0; j < field.length; j++) {
                    field[i][j] = dataFile.readChar();
                }
            }

            return field;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    char getWinner() {
        char winner = '-';
        if (isGameOver()) {
            try {
                dataFile.seek(0);
                winner = dataFile.readChar();
                dataFile.close();
                new File(fileName).delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return winner;
    }

}
