package container.array

open class SortedArrayContainerKotlin(capacity: Int) : ArrayContainerKotlin(capacity) {

    override fun insert(item: Long) {
        //array[++size] = item
        if (size == -1) {
            array[++size] = item
        } else if (size == 0) {
            if (array[0] > item) {
                insert(0, item)
            } else {
                insert(1, item)
            }
        } else {
            for (i in 0..size) {
                if (array[i] > item) {
                    insert(i, item)
                    break
                }
            }
            if (array[size] < item) {
                array[++size] = item
            }
        }
    }

    override fun toString(): String {
        val stringBuilder = StringBuilder()
        for (i in 0..size) {
            stringBuilder.append(array[i]).append(" ")
        }
        return stringBuilder.toString()
    }

}