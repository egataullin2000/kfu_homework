package container.array

class FastSortedArrayContainerKotlin(capacity: Int) : SortedArrayContainerKotlin(capacity) {

    override fun insert(item: Long) = when (size) {
        -1 -> array[++size] = item
        else -> insert(searchIndexFor(item), item)
    }

    private fun searchIndexFor(value: Long): Int {
        if (size == 0) {
            return if (array[0] > value) 0 else 1
        }
        var first = 0
        var last = size
        var pos = (first + last) / 2
        while ((array[pos] >= value) || (array[pos + 1] < value)) {
            when {
                first == last -> return last + 1
                value > array[pos] -> {
                    last = size
                    first = pos + 1
                    pos = (first + last) / 2
                }
                value < array[pos] -> {
                    last = pos
                    pos = (first + last) / 2
                }
                else -> return pos + 1
            }
        }

        return pos + 1
    }

}