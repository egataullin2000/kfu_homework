package container.array

import container.model.StudentKotlin

class StudentArrayContainerKotlin(capacity: Int) {

    private var array = arrayOfNulls<StudentKotlin>(capacity)
    private var size = -1

    fun insert(item: StudentKotlin) = when (size) {
        -1 -> array[++size] = item
        else -> insert(searchIndexFor(item), item)
    }

    fun get(index: Int) = if (index <= size) {
        array[index]
    } else {
        throw IndexOutOfBoundsException("Size index is smaller than requested in get() operation")
    }

    fun remove(index: Int) {
        if (index <= size) {
            for (i in index until size) {
                array[i] = array[i + 1]
            }
            array[size--] = null
        } else {
            throw IndexOutOfBoundsException("Size index is smaller than requested in remove() operation")
        }
    }

    fun remove(surname: String, name: String, patronymic: String) {
        for (i in 0..size) {
            val student = array[i]
            if (student?.surname.equals(surname)
                    && student?.name.equals(name)
                    && student?.patronymic.equals(patronymic)) {
                remove(i)
            }
        }
    }

    fun insert(index: Int, item: StudentKotlin) {
        if (index <= array.size) {
            for (i in ++size downTo index + 1) {
                array[i] = array[i - 1]
            }
            array[index] = item
        } else {
            throw IndexOutOfBoundsException("Size index is smaller than requested in insert() operation")
        }
    }

    private fun searchIndexFor(student: StudentKotlin): Int {
        val date = student.dateOfBirth
        if (size == 0) {
            return if (array[0]?.dateOfBirth!!.after(date)) 0 else 1
        }
        var first = 0
        var last = size
        var pos = (first + last) / 2
        while (array[pos]?.dateOfBirth!!.after(date) || array[pos + 1]?.dateOfBirth!!.before(date)) {
            when {
                first == last -> return last + 1
                date!!.after(array[pos]?.dateOfBirth) -> {
                    last = size
                    first = pos + 1
                    pos = (first + last) / 2
                }
                date.before(array[pos]?.dateOfBirth) -> {
                    last = pos
                    pos = (first + last) / 2
                }
                else -> return pos + 1
            }
        }

        return pos + 1
    }

    override fun toString(): String {
        val stringBuilder = StringBuilder("Students:\n")
        for (i in 0..size) {
            stringBuilder.append("\t").append(array[i]).append("\n")
        }

        return stringBuilder.toString()
    }

}