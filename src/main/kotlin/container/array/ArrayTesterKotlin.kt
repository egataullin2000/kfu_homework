package container.array

import java.util.*

fun main(args: Array<String>) {
    test(100)
    test(1000)
    test(100000)
}

fun test(capacity: Int) {
    println("Testing for $capacity items...\n")
    val sortedArrayContainer = SortedArrayContainerKotlin(capacity)
    val fastSortedArrayContainer = FastSortedArrayContainerKotlin(capacity)
    for (i in 0 until capacity) {
        val value = Random().nextLong()
        print("SortedArrayContainer: \t")
        var startTime = System.nanoTime()
        sortedArrayContainer.insert(value)
        notifyItemInserted(value, startTime)
        print("FastSortedArrayContainer: ")
        startTime = System.nanoTime()
        fastSortedArrayContainer.insert(value)
        notifyItemInserted(value, startTime)
    }
    println()
}

fun notifyItemInserted(value: Long, startTime: Long) {
    println("\titem $value inserted, it took ${System.nanoTime() - startTime} nanoseconds")
}