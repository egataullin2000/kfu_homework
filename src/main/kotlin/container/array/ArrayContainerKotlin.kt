package container.array

open class ArrayContainerKotlin(capacity: Int) {

    protected var array = LongArray(capacity)
    protected var size = -1

    open fun insert(item: Long) {
        array[++size] = item
    }

    fun get(index: Int) = if (index <= size) {
        array[index]
    } else {
        throw IndexOutOfBoundsException("Size index is smaller than requested in get() operation")
    }

    fun remove(index: Int) {
        if (index <= size) {
            for (i in index..size) {
                array[i] = array[i + 1]
            }
            array[size--] = 0
        } else {
            throw IndexOutOfBoundsException("Size index is smaller than requested in remove() operation")
        }
    }

    fun insert(index: Int, item: Long) {
        if (index <= array.size) {
            for (i in ++size downTo index + 1) {
                array[i] = array[i - 1]
            }
            array[index] = item
        } else {
            throw IndexOutOfBoundsException("Size index is smaller than requested in insert() operation")
        }
    }

    override fun toString(): String {
        val stringBuilder = StringBuilder()
        for (i in 0..size) {
            stringBuilder.append(array[i]).append(" ")
        }

        return stringBuilder.toString()
    }

}