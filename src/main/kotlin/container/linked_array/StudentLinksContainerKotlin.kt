package container.linked_array

import container.model.StudentKotlin

class StudentLinksContainerKotlin(firstStudent: StudentKotlin) : LinksContainerKotlin<StudentKotlin>(firstStudent) {

    fun remove(surname: String, name: String, patronymic: String) {
        val student = StudentKotlin(surname, name, patronymic)
        if (starterLink?.value == student) {
            starterLink = starterLink!!.nextLink
        } else {
            var currentLink = starterLink
            while (currentLink?.nextLink?.value != student) {
                currentLink = currentLink?.nextLink
            }
            currentLink.nextLink = currentLink.nextLink!!.nextLink
        }
    }

}