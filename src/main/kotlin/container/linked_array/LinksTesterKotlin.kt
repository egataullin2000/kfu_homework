package container.linked_array

import container.model.StudentKotlin
import java.util.*

fun main(args: Array<String>) {
    println("Testing LinksContainer:")
    val linksContainer = LinksContainerKotlin<Long>(20)
    linksContainer.insert(10)
    linksContainer.insert(30)
    println("\t$linksContainer")
    linksContainer.removeFirst()
    println("\t$linksContainer")

    println("Testing SortedLinksContainer")
    val sortedLinksContainer = SortedLinksContainerKotlin<Long>(3)
    sortedLinksContainer.insert(1)
    println("\t$sortedLinksContainer")
    //sortedLinksContainer.insert(20)
    //sortedLinksContainer.insert(1)
    sortedLinksContainer.removeFirst()
    println("\t$sortedLinksContainer")

    println("Testing BidirectionalLinksContainer")
    val bidirectionalLinkContainer = BidirectionalLinkContainerKotlin<Long>(3)
    bidirectionalLinkContainer.insert(1)
    println("\t$bidirectionalLinkContainer")
    //bidirectionalLinkContainer.insert(20)
    //bidirectionalLinkContainer.insert(1)
    //bidirectionalLinkContainer.removeFirst()
    bidirectionalLinkContainer.remove(3)
    println("\t$bidirectionalLinkContainer")

    println("Testing StudentLinksContainer")
    val students =
            StudentLinksContainerKotlin(StudentKotlin("1", "1", "1", Date("1 Apr 2000")))
    students.insert(StudentKotlin("2", "2", "2", Date("1 March 2000")))
    println("\t$students")
    students.remove("1", "1", "1")
    println("\t$students")
}