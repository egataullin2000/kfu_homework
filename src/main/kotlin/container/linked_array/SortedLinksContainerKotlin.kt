package container.linked_array

import container.model.LinkKotlin

open class SortedLinksContainerKotlin<E : Comparable<E>>(firstValue: E) : LinksContainerKotlin<E>(firstValue) {

    override fun insert(value: E) {
        val newLink = LinkKotlin(value)
        when {
            starterLink == null -> starterLink = newLink
            newLink.value <= starterLink!!.value -> {
                newLink.nextLink = starterLink
                starterLink = newLink
            }
            else -> {
                var currentLink = starterLink
                do {
                    if (newLink.value >= currentLink!!.value) {
                        newLink.nextLink = currentLink.nextLink
                        currentLink.nextLink = newLink
                        break
                    }
                    currentLink = currentLink.nextLink!!
                } while (currentLink!!.hasNext())
            }
        }
    }

}