package container.linked_array

import container.model.LinkKotlin

open class LinksContainerKotlin<E : Comparable<E>>(firstValue: E) {

    protected var starterLink: LinkKotlin<E>?

    init {
        starterLink = LinkKotlin(firstValue)
    }

    open fun insert(value: E) {
        val newLink = LinkKotlin(value)
        if (starterLink == null) {
            starterLink = newLink
        } else {
            var currentLink = starterLink
            while (currentLink!!.hasNext()) {
                currentLink = currentLink.nextLink
            }
            currentLink.nextLink = newLink
        }
    }

    fun display() {
        println(this)
    }

    override fun toString(): String {
        val stringBuilder = StringBuilder()
        var currentLink = starterLink
        stringBuilder.append(currentLink?.value).append(" ")
        while (currentLink != null && currentLink.hasNext()) {
            currentLink = currentLink.nextLink!!
            stringBuilder.append(currentLink.value).append(" ")
        }

        return stringBuilder.toString()
    }

    fun removeFirst() {
        if (starterLink!!.hasNext()) {
            starterLink = starterLink!!.nextLink!!
        } else {
            starterLink = null
        }
    }

}