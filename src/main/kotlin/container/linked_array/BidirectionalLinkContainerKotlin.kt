package container.linked_array

import container.model.BidirectionalLinkKotlin

class BidirectionalLinkContainerKotlin<E : Comparable<E>>(firstValue: E) {

    private var starterLink: BidirectionalLinkKotlin<E>?
    private var lastLink: BidirectionalLinkKotlin<E>?

    init {
        starterLink = BidirectionalLinkKotlin(firstValue)
        lastLink = starterLink
    }

    fun insert(value: E) {
        val newLink = BidirectionalLinkKotlin(value)
        when {
            starterLink == null -> {
                starterLink = newLink
                lastLink = starterLink
            }
            newLink.value <= starterLink!!.value -> {
                newLink.nextLink = starterLink
                starterLink?.prevLink = newLink
                starterLink = newLink
            }
            else -> {
                var currentLink = starterLink
                do {
                    if (newLink.value >= currentLink!!.value) {
                        newLink.prevLink = currentLink
                        newLink.nextLink = currentLink.nextLink
                        if (newLink.nextLink == null) lastLink = newLink
                        currentLink.nextLink = newLink
                        break
                    }
                    currentLink = currentLink.nextLink!!
                } while (currentLink!!.hasNext())
            }
        }
    }

    fun removeFirst() {
        if (starterLink != null && starterLink!!.hasNext()) {
            starterLink = starterLink!!.nextLink!!
        } else {
            starterLink = null
        }
    }

    fun remove(value: E) {
        var currentLink = starterLink
        while (currentLink?.value != value) {
            currentLink = currentLink?.nextLink
        }
        if (currentLink.nextLink == null) lastLink = currentLink.prevLink
        currentLink.prevLink?.nextLink = currentLink.nextLink
        currentLink.nextLink?.prevLink = currentLink.prevLink
    }

    fun display(fromStart: Boolean) = if (fromStart) {
        println(this)
    } else {
        println(toStringWithRevers())
    }

    private fun toStringWithRevers(): String {
        val stringBuilder = StringBuilder()
        var currentLink = lastLink
        stringBuilder.append(currentLink?.value).append(" ")
        while (currentLink?.hasPrev() == true) {
            currentLink = currentLink.prevLink!!
            stringBuilder.append(currentLink.value).append(" ")
        }

        return stringBuilder.toString()
    }

    override fun toString(): String {
        val stringBuilder = StringBuilder()
        var currentLink = starterLink
        stringBuilder.append(currentLink?.value).append(" ")
        while (currentLink?.hasNext() == true) {
            currentLink = currentLink.nextLink!!
            stringBuilder.append(currentLink.value).append(" ")
        }

        return stringBuilder.toString()
    }

}