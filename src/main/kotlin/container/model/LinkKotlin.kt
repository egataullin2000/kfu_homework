package container.model

data class LinkKotlin<E : Comparable<E>>(var value: E, var nextLink: LinkKotlin<E>? = null) {
    fun hasNext() = nextLink != null
    override fun toString() = value.toString()
}