package container.model

data class BidirectionalLinkKotlin<E : Comparable<E>>(
        var value: E,
        var prevLink: BidirectionalLinkKotlin<E>? = null,
        var nextLink: BidirectionalLinkKotlin<E>? = null
) {
    fun hasPrev() = prevLink != null

    fun hasNext() = nextLink != null

    override fun toString() = value.toString()
}