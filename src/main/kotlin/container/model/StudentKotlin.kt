package container.model

import java.util.*

data class StudentKotlin(
        val surname: String? = null,
        val name: String? = null,
        val patronymic: String? = null,
        var dateOfBirth: Date? = null
) : Comparable<StudentKotlin> {
    override fun toString() = "$surname $name $patronymic, $dateOfBirth"

    override fun compareTo(other: StudentKotlin) = dateOfBirth!!.compareTo(other.dateOfBirth)

    override fun equals(other: Any?) = if (other is StudentKotlin) {
        other.surname.equals(surname) && other.name.equals(name) && other.patronymic.equals(patronymic)
    } else false

    override fun hashCode(): Int {
        return super.hashCode()
    }
}