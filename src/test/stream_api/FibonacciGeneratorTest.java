package stream_api;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FibonacciGeneratorTest extends TestCase {
    public void testSequence() {
        List<Integer> list = new ArrayList<>(10);
        assertEquals(FibonacciGenerator.generate(0), list);
        list.add(1);
        assertEquals(FibonacciGenerator.generate(1), list);
        list.addAll(Arrays.asList(1, 2, 3, 5));
        assertEquals(FibonacciGenerator.generate(5), list);
        list.addAll(Arrays.asList(8, 13, 21, 34, 55));
        assertEquals(FibonacciGenerator.generate(10), list);
    }

    public void testSequenceProduct() {
        int[] n = new int[]{1, 5, 10};
        int[] requested = new int[]{1, 30, 122522400};
        for (int i = 0; i < n.length; i++) {
            assertEquals(FibonacciGenerator.product(n[i]), requested[i]);
        }
    }
}