package container.list;

import junit.framework.TestCase;

public class LinkedListTest extends TestCase {

    private LinkedList<Integer> actual = new LinkedList<>();
    private java.util.LinkedList<Integer> expected = new java.util.LinkedList<>();

    public void test() {
        addToLists(0);
        assert(equals(actual, expected));
        addToLists(-5);
        assert(equals(actual, expected));
        removeFromLists(0);
        assert(equals(actual, expected));
        addToLists(1, 7);
        assert(equals(actual, expected));
        removeFromLists(1);
        assert(equals(actual, expected));
    }

    private void addToLists(int item) {
        addToLists(0, item);
    }

    private void addToLists(int index, int item) {
        actual.add(index, item);
        expected.add(index, item);
    }

    private void removeFromLists(int index) {
        actual.remove(index);
        expected.remove(index);
    }

    private boolean equals(LinkedList list1, java.util.LinkedList list2) {
        if (list1.size() != list2.size()) return false;
        for (int i = 0; i < list1.size(); i++) {
            if (list1.get(i) != list2.get(i)) return false;
        }

        return true;
    }

}