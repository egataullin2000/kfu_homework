package container.list;

import junit.framework.TestCase;

public class ArrayListTest extends TestCase {

    private ArrayList<Integer> list1 = new ArrayList<>(Integer.class);
    private java.util.ArrayList<Integer> list2 = new java.util.ArrayList<>();

    public void test() {
        addToLists(0);
        assert (equals(list1, list2));
        addToLists(-5);
        assert (equals(list1, list2));
        removeFromLists(0);
        assert (equals(list1, list2));
        addToLists(0, 7);
        assert (equals(list1, list2));
    }

    private void addToLists(int item) {
        addToLists(0, item);
    }

    private void addToLists(int index, int item) {
        list1.add(index, item);
        list2.add(index, item);
    }

    private void removeFromLists(int index) {
        list1.remove(index);
        list2.remove(index);
    }

    private boolean equals(ArrayList list1, java.util.ArrayList list2) {
        if (list1.size() != list2.size()) return false;
        for (int i = 0; i < list1.size(); i++) {
            if (list1.get(i) != list2.get(i)) return false;
        }

        return true;
    }

}